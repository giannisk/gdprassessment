<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Home
 */
Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'home',
]);

/**
 * Terms of Use
 */
Route::get('/termsofuse', [
    'uses' => 'HomeController@indexTermsOfUse',
    'as' => 'home.termsofuse',
]);

/**
 * Privacy Policy
 */
Route::get('/privacypolicy', [
    'uses' => 'HomeController@indexPrivacyPolicy',
    'as' => 'home.privacypolicy',
]);

/**
 * Documentation
 */
Route::get('/documentation', [
    'uses' => 'DocumentationController@index',
    'as' => 'documentation',
]);

/**
 * Knowledge Base
 */
Route::get('/knowledgebase', [
    'uses' => 'KnowledgeBaseController@index',
    'as' => 'knowledgebase',
]);

/**
 * Authentication
 */
Route::get('/signup', [
    'uses' => 'Auth\AuthController@getSignup',
    'as' => 'auth.signup',
    'middleware' => ['guest'],
]);

Route::post('/signup', [
    'uses' => 'Auth\AuthController@postSignup',
    'middleware' => ['guest'],
]);

Route::get('/verify/{verification_token}', [
    'uses' => 'Auth\AuthController@verifyEmail',
    'as' => 'verify',
]);

Route::get('/signin', [
    'uses' => 'Auth\AuthController@getSignin',
    'as' => 'auth.signin',
]);

Route::post('/signin', [
    'uses' => 'Auth\AuthController@postSignin',
]);

Route::get('/signout', [
    'uses' => 'Auth\AuthController@getSignout',
    'as' => 'auth.signout',
    'middleware' => ['auth', 'verified'],
]);

Route::get('/forgotpassword', [
    'uses' => 'Auth\ForgotPasswordController@getResetForm',
    'as' => 'auth.forgotpassword',
    'middleware' => ['guest'],
]);

Route::post('/forgotpassword', [
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail',
    'middleware' => ['guest'],
]);

Route::get('/resetpassword/{token}', [
    'uses' => 'Auth\ResetPasswordController@showResetForm',
    'as' => 'auth.resetpassword',
    'middleware' => ['guest'],
]);

Route::post('/resetpassword/{token}', [
    'uses' => 'Auth\ResetPasswordController@reset',
    'middleware' => ['guest'],
]);

/**
 * Dashboard
 */
Route::get('/dashboard', [
    'uses' => 'Dashboard\DashboardController@index',
    'as' => 'dashboard',
    'middleware' => ['verified'],
]);

/**
 * Account Settings
 */
Route::get('/dashboard/accountsettings', [
    'uses' => 'Dashboard\AccountSettingsController@index',
    'as' => 'dashboard.accountsettings',
    'middleware' => ['verified'],
]);

Route::post('/dashboard/accountsettings', [
    'uses' => 'Dashboard\AccountSettingsController@updateAccount',
    'middleware' => ['verified'],
]);

Route::post('/dashboard/accountsettings/deleteaccount', [
    'uses' => 'Dashboard\AccountSettingsController@deleteAccount',
    'as' => 'dashboard.accountsettings.deleteaccount',
    'middleware' => ['verified'],
]);

/**
 * Processing Activities
 */
Route::get('/dashboard/processingactivity', [
    'uses' => 'Dashboard\ProcessingActivityController@index',
    'as' => 'dashboard.processingactivity',
    'middleware' => ['verified'],
]);

Route::post('/dashboard/processingactivity', [
    'uses' => 'Dashboard\ProcessingActivityController@saveProcessingActivity',
    'middleware' => ['verified'],
]);

Route::get('/dashboard/processingactivity/{processingactivityid}', [
    'uses' => 'Dashboard\ProcessingActivityController@viewProcessingActivity',
    'as' => 'dashboard.view.processingactivity',
    'middleware' => ['verified'],
]);


/**
 * GDPR Assessment
 */
Route::get('/dashboard/gdprassessment', [
    'uses' => 'Dashboard\GDPRAssessmentController@index',
    'as' => 'dashboard.gdprassessment',
    'middleware' => ['verified'],
]);

Route::post('/dashboard/gdprassessment', [
    'uses' => 'Dashboard\GDPRAssessmentController@saveGDPRAssessment',
    'middleware' => ['verified'],
]);

Route::get('/dashboard/gdprassessment/{gdprassessmentid}', [
    'uses' => 'Dashboard\GDPRAssessmentController@viewGDPRAssessment',
    'as' => 'dashboard.view.gdprassessment',
    'middleware' => ['verified'],
]);

/*
Route::post('/dashboard/gdprassessment', [
    'uses' => 'Dashboard\GDPRAssessmentController@deleteGDPRAssessment',
    'as' => 'dashboard.delete.gdprassessment',
    'middleware' => ['verified'],
]); */

/**
 * DPI Assessment
 */
Route::get('/dashboard/dpiassessment', [
    'uses' => 'Dashboard\DPIAssessmentController@index',
    'as' => 'dashboard.dpiassessment',
    'middleware' => ['verified'],
]);

Route::post('/dashboard/dpiassessment', [
    'uses' => 'Dashboard\DPIAssessmentController@saveDPIAssessment',
    'middleware' => ['verified'],
]);

Route::get('/dashboard/dpiassessment/{dpiassessmentid}', [
    'uses' => 'Dashboard\DPIAssessmentController@viewDPIAssessment',
    'as' => 'dashboard.view.dpiassessment',
    'middleware' => ['verified'],
]);

Route::post('/dashboard/dpiassessment/{dpiassessmentid}/validate', [
    'uses' => 'Dashboard\DPIAssessmentController@validateDPIAssessment',
    'as' => 'dashboard.validate.dpiassessment',
    'middleware' => ['verified'],
]);

/**
 * Completed Assessments
 */
Route::get('/dashboard/completedassessments', [
    'uses' => 'Dashboard\CompletedAssessmentsController@index',
    'as' => 'dashboard.completedassessments',
    'middleware' => ['verified'],
]);
