# GDPR Compliance Assessment

This is an open-source web-based system that helps organisations to align themselves with the EU GDPR. Users can keep records of processing activities and conduct GDPR and DPI assessments.

![Completed Assessments Preview](preview/completedassessments_preview.png)

[Online Demo](https://demo.gdprassessment.io)

## Quickstart Guide

### Using Homestead

1. Map the application within `Homestead.yaml`.
2. Create a new `.env` file and configure the application's environment. The included `.env.example` file provides essential guidance.
3. Install necessary depedencies using `composer install`.
4. Run the database migrations using `php artisan migrate`.
5. Generate the application's encryption key using `php artisan key:generate`.

For additional information regarding Laravel and Homestead, please consult the official [documentation](https://laravel.com/docs/).

This Laravel application integrates with [Sendgrid](https://sendgrid.com/) for sending verification e-mails and helping users perform password resets. Don't forget to specify your Sendgrid API key within the `.env` file.

## License

Final work is made available under the [GNU Affero General Public License](LICENSE).

## Disclaimer

This web-based system serves informational purposes only and does not provide legal advice. Please contact an attorney for consultation regarding your particular situation. We are not responsible for any damages that arise in connection with the use of this web-based system.
