<?php

namespace GDPR_Compliance_Assessment\Notifications;

use GDPR_Compliance_Assessment\Models\User;
use GDPR_Compliance_Assessment\Mail\SendgridMail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Sichikawa\LaravelSendgridDriver\Sendgrid;

class ResetPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->subject('Reset Your Password')
                    ->greeting('Hello '.$notifiable->first_name.' '.$notifiable->last_name.'!')
                    ->line('Please click the button below to reset your password.')
                    ->action('Reset Password', route('auth.resetpassword', ['token' => $this->token]))
                    ->line('If you did not request a password reset, no further action is required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
