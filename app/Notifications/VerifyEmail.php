<?php

namespace GDPR_Compliance_Assessment\Notifications;

use GDPR_Compliance_Assessment\Models\User;
use GDPR_Compliance_Assessment\Mail\SendgridMail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Sichikawa\LaravelSendgridDriver\Sendgrid;

class VerifyEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Verify Your E-mail Address')
                    ->greeting('Hello '.$notifiable->first_name.' '.$notifiable->last_name.'!')
                    ->line('Please now verify your e-mail address to continue.')
                    ->action('Verify E-mail Address', route('verify', $notifiable->verification_token))
                    ->line('Thank you for using our application.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
