<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Auth;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function getResetForm()
    {
        return view('auth.forgotpassword');
    }

    protected function sendResetLinkResponse($response)
    {
        return redirect()->back()->with('success', 'Email sent! Please check your inbox.');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
