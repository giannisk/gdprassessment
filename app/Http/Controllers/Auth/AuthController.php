<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Auth;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use GDPR_Compliance_Assessment\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class AuthController extends Controller
{

    /* 
    ** Sign Up
    */
    public function getSignup()
    {
        return view('auth.signup');
    }

    public function postSignup(Request $request)
    {
        $messages = [
            'first_name.required' => 'Please enter your first name.',
            'last_name.required' => 'Please enter your last name.',
            'email.required' => 'Please enter your e-mail address.',
            'username.required' => 'Please enter your desired username.',
            'password.required' => 'Please enter your desired password.',
            'checkbox.required' => 'Please accept the terms of use and privacy policy.',
        ];

        $this->validate($request, [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'email' => 'required|unique:users|email|max:255',
            'username' => 'required|unique:users|alpha_dash|max:20',
            'password' => 'required|confirmed|min:8',
            'checkbox' => 'required',
        ], $messages);

        $uuid1 = Uuid::uuid1();

        $user = User::create([
            'uuid' => $uuid1->toString(),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
            'verification_token' => str_random(30),
        ]);

        try {
            $user->sendVerificationEmail();
        }
        catch (Exception $e){
            return redirect()->back()->with('danger', 'There currently seems to be an issue with the e-mail service. Please try again later.');
        }

        return redirect()->back()->with('warning', 'We have sent you an e-mail with instructions to verify your e-mail address. You cannot sign-in unless you confirm your e-mail address.');
    }

    public function verifyEmail($verification_token)
    {
        try {
            User::where('verification_token', $verification_token)->firstOrFail()
                ->update(['verification_token' => null]);

        return redirect()->route('auth.signin')->with('success', 'Your account has now been verified.');
        }
        catch (Exception $e){
            return redirect()->back()->with('danger', 'We are currently unable to verify your e-mail address. Please try again later.');
        }
    }

    /* 
    ** Sign In
    */
    public function getSignin()
    {

        if (Auth::check()) 
        {
            return redirect()->route('dashboard');
        }

        return view('auth.signin');

    }

    public function postSignin(Request $request)
    {
        $messages = [
            'email.required' => 'Please enter your e-mail address.',
            'password.required' => 'Please enter your password.',
        ];

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ], $messages);

        if (!Auth::attempt($request->only(['email', 'password']), $request->has('remember'))) {
            return redirect()->back()->with('danger', 'The credentials you entered were invalid.');
        }

        return redirect()->route('dashboard');
    }

    /* 
    ** Sign Out
    */
    public function getSignout()
    {
        Auth::logout();

        return redirect()->route('home');
    }

    /* 
    ** Password Reset
    */
    public function forgotPassword()
    {
        return view('auth.forgotpassword');
    }

}
