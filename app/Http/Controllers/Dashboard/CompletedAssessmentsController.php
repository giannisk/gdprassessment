<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Dashboard;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use GDPR_Compliance_Assessment\Models\ProcessingActivity;
use GDPR_Compliance_Assessment\Models\GDPRAssessment;
use GDPR_Compliance_Assessment\Models\DPIAssessment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompletedAssessmentsController extends Controller
{
    public function index()
    {
        $processingactivities = ProcessingActivity::where('username', '=', Auth::user()->username)->get();
        $gdprassessments = GDPRAssessment::where('username', '=', Auth::user()->username)->get();
        $dpiassessments = DPIAssessment::where('username', '=', Auth::user()->username)->get();
        return view('dashboard.completedassessments', compact('processingactivities', 'gdprassessments', 'dpiassessments'));
    }
}
