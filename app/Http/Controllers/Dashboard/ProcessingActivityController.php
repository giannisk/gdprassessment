<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Dashboard;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use GDPR_Compliance_Assessment\Models\ProcessingActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class ProcessingActivityController extends Controller
{
    public function index()
    {
        return view('dashboard.processingactivity');
    }

    public function saveProcessingActivity(Request $request)
    {
        $messages = [
            'required' => 'This field is required.',
        ];

        $this->validate($request, [
            'first_name' => 'required', 'max:32',
            'last_name' => 'required', 'max:32',
            'organisation_name' => 'required', 'max:32',
            'job_title' => 'required', 'max:32',
            'processingactivity_name' => 'required', 'max:32',
            'controller_name' => 'required', 'max:32',
            'processor_name' => 'required', 'max:32',
            'processingactivity_description' => 'required', 'max:255',
            'storage_method' => 'required', 'max:32',
            'data_type' => 'required', 'max:32',
            'legal_justification' => 'required', 'max:32',
            'security_measures' => 'required', 'max:32',
            'processing_principles' => 'required', 'max:32',
            'data_subject_rights' => 'required', 'max:32',
        ], $messages);

        $processingactivity_uuid = Uuid::uuid1();
        $user = Auth::user();

        $processingactivity = ProcessingActivity::create([
            'uuid' => $processingactivity_uuid->toString(),
            'username' => $user->username,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'organisation_name' => $request->input('organisation_name'),
            'job_title' => $request->input('job_title'),
            'processingactivity_name' => $request->input('processingactivity_name'),
            'controller_name' => $request->input('controller_name'),
            'processor_name' => $request->input('processor_name'),
            'processingactivity_description' => $request->input('processingactivity_description'),
            'storage_method' => $request->input('storage_method'),
            'data_type' => $request->input('data_type'),
            'legal_justification' => $request->input('legal_justification'),
            'security_measures' => $request->input('security_measures'),
            'processing_principles' => $request->input('processing_principles'),
            'data_subject_rights' => $request->input('data_subject_rights'),
        ]);

        return redirect('/dashboard/processingactivity/' . $processingactivity_uuid->toString());

    }

    public function viewProcessingActivity(Request $request, $uuid)
    {
        if (!Uuid::isValid($uuid)) {
            return abort(404);
        }

        $processingactivity_uuid = ProcessingActivity::where('uuid', $uuid)->get()->first();
        $processingactivity = ProcessingActivity::where('uuid', $uuid and 'username', Auth::user()->username)->get();

        return view('dashboard.view.processingactivity', ['processingactivity' => $processingactivity_uuid], compact('processingactivity'));
    }
}
