<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Dashboard;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use GDPR_Compliance_Assessment\Models\User;
use GDPR_Compliance_Assessment\Models\ProcessingActivity;
use GDPR_Compliance_Assessment\Models\GDPRAssessment;
use GDPR_Compliance_Assessment\Models\DPIAssessment;
use Auth;
use Illuminate\Http\Request;

class AccountSettingsController extends Controller
{
    public function index()
    {
        return view('dashboard.accountsettings');
    }

    public function updateAccount(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'max:20',
            'last_name' => 'max:20',
            'email' => 'email|max:255',
            'password' => 'confirmed|min:8',
        ]);

        Auth::user()->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        return redirect()->route('dashboard.accountsettings');
    }

    public function deleteAccount(Request $request)
    {
        $user = Auth::user();

        if ($request->has('delete_entire_account_data')) {
            $processingactivities = ProcessingActivity::where('username', $user->username)->delete();
            $gdprassessments = GDPRAssessment::where('username', $user->username)->delete();
            $dpiassessments = DPIAssessment::where('username', $user->username)->delete();
            if ($user->delete()) {
              return redirect()->route('home');
            }
        }

        else {
            if ($user->delete()) {
              return redirect()->route('home');
            }
        }
    }
}
