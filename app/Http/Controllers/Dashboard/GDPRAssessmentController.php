<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Dashboard;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use GDPR_Compliance_Assessment\Models\ProcessingActivity;
use GDPR_Compliance_Assessment\Models\GDPRAssessment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DB;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class GDPRAssessmentController extends Controller
{
    public function index()
    {
        $processingactivities = ProcessingActivity::where('username', '=', Auth::user()->username)->get();
        return view('dashboard.gdprassessment', compact('processingactivities'));
    }

    public function saveGDPRAssessment(Request $request)
    {
        $messages = [
            'required' => 'This field is required.',
            'required_if' => 'This field is required.',
        ];

        $this->validate($request, [
            'first_name' => 'required', 'max:32',
            'last_name' => 'required', 'max:32',
            'organisation_name' => 'required', 'max:32',
            'job_title' => 'required', 'max:32',
            'organisation_role' => 'required', 'max:64',
            'EU_data_subjects' => 'required', 'max:64',
            'technical_organisational_measures' => 'required', 'max:64',
            'records_processing_activities' => 'required', 'max:64',
            'privacy_policy' => 'required', 'max:64',
            'data_protection_design_default' => 'required', 'max:64',
            'data_breach_procedure' => 'required', 'max:64',
            'data_protection_officer' => 'required', 'max:64',
        ], $messages);

        $gdprassessment_uuid = Uuid::uuid1();
        $user = Auth::user();

        $gdprassessment = GDPRAssessment::create([
            'uuid' => $gdprassessment_uuid->toString(),
            'username' => $user->username,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'organisation_name' => $request->input('organisation_name'),
            'job_title' => $request->input('job_title'),
            'organisation_role' => $request->input('organisation_role'),
            'EU_data_subjects' => $request->input('EU_data_subjects'),
            'technical_organisational_measures' => $request->input('technical_organisational_measures'),
            'records_processing_activities' => $request->input('records_processing_activities'),
            'privacy_policy' => $request->input('privacy_policy'),
            'data_protection_design_default' => $request->input('data_protection_design_default'),
            'data_breach_procedure' => $request->input('data_breach_procedure'),
            'data_protection_officer' => $request->input('data_protection_officer'),
        ]);

        $gdprassessment = GDPRAssessment::find($gdprassessment_uuid);

        $processingactivityscore = 4;
        if ($request->has('selected_processing_activities')) {
            $selected_processing_activities = $request->input('selected_processing_activities');
            foreach ($selected_processing_activities as $selected_processing_activity) {
                $processingactivity = ProcessingActivity::find($selected_processing_activity);
                $gdprassessment->processingactivities()->attach($processingactivity);
                if ($processingactivity->legal_justification == "None") $processingactivityscore -= 1;
                if ($processingactivity->security_measures == "No, my organisation has not implemented any technical and organisational measures.") $processingactivityscore -= 1;
                if ($processingactivity->processing_principles == "No, my organisation does not uphold any processing principle.") $processingactivityscore -= 1;
                if ($processingactivity->data_subject_rights == "No, my organisation does not help data subjects exercise their rights.") $processingactivityscore -= 1;
            }
        }

        $gdprassessmentscore = 6;

        if ($gdprassessment->technical_organisational_measures == "No, my organisation has not implemented appropriate technical and organisational measures for the safeguarding of personal data.") $gdprassessmentscore -= 1;
        if ($gdprassessment->records_processing_activities == "No, my organisation does not maintain records of processing activities.") $gdprassessmentscore -= 1;
        if ($gdprassessment->privacy_policy == "No, there isn't such a document published anywhere.") $gdprassessmentscore -= 1;
        if ($gdprassessment->data_protection_design_default == "No, my organisation does not embrace data protection by design and by default.") $gdprassessmentscore -= 1;
        if ($gdprassessment->data_breach_procedure == "No, my organisation has not established a procedure in the event of a data breach.") $gdprassessmentscore -= 1;
        if ($gdprassessment->data_protection_officer == "No, my organisation has not appointed a data protection officer.") $gdprassessmentscore -= 1;

        if ($gdprassessmentscore + $processingactivityscore >= 10) $gdprassessment->status = "Compliant";
        elseif ($gdprassessmentscore + $processingactivityscore >= 8) $gdprassessment->status = "Semi-Compliant";
        else $gdprassessment->status = "Non-Compliant";
        $gdprassessment->save();

        return redirect('/dashboard/gdprassessment/' . $gdprassessment_uuid->toString());
    }

    public function viewGDPRAssessment(Request $request, $uuid)
    {
        try {
            if (!Uuid::isValid($uuid)) {
                return abort(404);
            }

            $gdprassessment_uuid = GDPRAssessment::where('uuid', $uuid)->get()->first();
            $gdprassessment = GDPRAssessment::with('processingactivities')->where('uuid', $uuid and 'username', Auth::user()->username)->get();
            return view('dashboard.view.gdprassessment', ['gdprassessment' => $gdprassessment_uuid], compact('gdprassessment'));
        }
        catch (Exception $e){
            return abort(404);
        }

    }

    public function deleteGDPRAssessment(Request $request)
    {
        $gdprassessment_uuid = $request->segments()[1];
        $gdprassessment = GDPRAssessment::findOrFail($gdprassessment_uuid); 

        if ($gdprassessment->delete()) {
          return redirect()->route('dashboard');
        }
    }
}
