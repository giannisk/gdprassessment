<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Dashboard;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use GDPR_Compliance_Assessment\Models\DPIAssessment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class DPIAssessmentController extends Controller
{
    public function index()
    {
        return view('dashboard.dpiassessment');
    }

    public function saveDPIAssessment(Request $request)
    {
        $messages = [
            'required' => 'This field is required.',
        ];

        $this->validate($request, [
            'first_name' => 'required', 'max:32',
            'last_name' => 'required', 'max:32',
            'organisation_name' => 'required', 'max:32',
            'job_title' => 'required', 'max:32',
            'data_processing' => 'required', 'max:256',
            'responsibilities' => 'required', 'max:256',
            'relevant_standards' => 'required', 'max:256',
            'data_involved' => 'required', 'max:256',
            'data_life_cycle' => 'required', 'max:256',
            'data_supporting_assets' => 'required', 'max:256',
            'explicitness_and_legitimacy' => 'required', 'max:256',
            'lawfulness' => 'required', 'max:256',
            'data_minimisation' => 'required', 'max:256',
            'data_accuracy' => 'required', 'max:256',
            'storage_duration' => 'required', 'max:256',
            'communication' => 'required', 'max:256',
            'access_and_portability' => 'required', 'max:256',
            'rectification_and_erasure' => 'required', 'max:256',
            'restriction_and_object' => 'required', 'max:256',
            'contract_governance' => 'required', 'max:256',
            'international_data_transfer' => 'required', 'max:256',
            'additional_information' => 'required', 'max:256',
            'security_measures' => 'required', 'max:256',
            'r1_impact_data_subjects' => 'required', 'max:256',
            'r1_main_threats' => 'required', 'max:256',
            'r1_risk_sources' => 'required', 'max:256',
            'r1_risk_severity' => 'required', 'max:16',
            'r1_risk_likelihood' => 'required', 'max:16',
            'r2_impact_data_subjects' => 'required', 'max:256',
            'r2_main_threats' => 'required', 'max:256',
            'r2_risk_sources' => 'required', 'max:256',
            'r2_risk_severity' => 'required', 'max:16',
            'r2_risk_likelihood' => 'required', 'max:16',
            'r3_impact_data_subjects' => 'required', 'max:256',
            'r3_main_threats' => 'required', 'max:256'  ,
            'r3_risk_sources' => 'required', 'max:256',
            'r3_risk_severity' => 'required', 'max:16',
            'r3_risk_likelihood' => 'required', 'max:16',
        ], $messages);

        $uuid1 = Uuid::uuid1();
        $user = Auth::user();

        $dpiassessment = DPIAssessment::create([
            'uuid' => $uuid1->toString(),
            'username' => $user->username,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'organisation_name' => $request->input('organisation_name'),
            'job_title' => $request->input('job_title'),
            'data_processing' => $request->input('data_processing'),
            'responsibilities' => $request->input('responsibilities'),
            'relevant_standards' => $request->input('relevant_standards'),
            'data_involved' => $request->input('data_involved'),
            'data_life_cycle' => $request->input('data_life_cycle'),
            'data_supporting_assets' => $request->input('data_supporting_assets'),
            'explicitness_and_legitimacy' => $request->input('explicitness_and_legitimacy'),
            'lawfulness' => $request->input('lawfulness'),
            'data_minimisation' => $request->input('data_minimisation'),
            'data_accuracy' => $request->input('data_accuracy'),
            'storage_duration' => $request->input('storage_duration'),
            'communication' => $request->input('communication'),
            'access_and_portability' => $request->input('access_and_portability'),
            'rectification_and_erasure' => $request->input('rectification_and_erasure'),
            'restriction_and_object' => $request->input('restriction_and_object'),
            'contract_governance' => $request->input('contract_governance'),
            'international_data_transfer' => $request->input('international_data_transfer'),
            'additional_information' => $request->input('additional_information'),
            'security_measures' => $request->input('security_measures'),
            'r1_impact_data_subjects' => $request->input('r1_impact_data_subjects'),
            'r1_main_threats' => $request->input('r1_main_threats'),
            'r1_risk_sources' => $request->input('r1_risk_sources'),
            'r1_risk_severity' => $request->input('r1_risk_severity'),
            'r1_risk_likelihood' => $request->input('r1_risk_likelihood'),
            'r2_impact_data_subjects' => $request->input('r2_impact_data_subjects'),
            'r2_main_threats' => $request->input('r2_main_threats'),
            'r2_risk_sources' => $request->input('r2_risk_sources'),
            'r2_risk_severity' => $request->input('r2_risk_severity'),
            'r2_risk_likelihood' => $request->input('r2_risk_likelihood'),
            'r3_impact_data_subjects' => $request->input('r3_impact_data_subjects'),
            'r3_main_threats' => $request->input('r3_main_threats'),
            'r3_risk_sources' => $request->input('r3_risk_sources'),
            'r3_risk_severity' => $request->input('r3_risk_severity'),
            'r3_risk_likelihood' => $request->input('r3_risk_likelihood'),
        ]);

        return redirect()->route('dashboard.view.dpiassessment', [$uuid1]);

    }

    public function viewDPIAssessment(Request $request, $uuid)
    {
        if (!Uuid::isValid($uuid)) {
            return abort(404);
        }

        $dpiassessment_uuid = DPIAssessment::where('uuid', $uuid)->get()->first();
        $dpiassessment = DPIAssessment::where('uuid', $uuid and 'username', Auth::user()->username)->get();

        return view('dashboard.view.dpiassessment', ['dpiassessment' => $dpiassessment_uuid], compact('dpiassessment'));
    }

    public function validateDPIAssessment(Request $request, $uuid)
    {
        $dpiassessment = DPIAssessment::where('uuid', $uuid)->get()->first();

        if (!$dpiassessment) {
            return redirect()->back();
        }

        $messages = [
            'required' => 'This field is required.',
        ];

        $this->validate($request, [
            'additional_measures' => 'required', 'max:256',
            'dpo_opinion' => 'required', 'max:256',
        ], $messages);

        DPIAssessment::query()->where('uuid', $uuid)->update(['additional_measures' => $request->input('additional_measures'), 'dpo_opinion' => $request->input('dpo_opinion')]);

        switch ($request->input('validate')) {
            case 'reject':
                if ($dpiassessment->status == null) {
                    DPIAssessment::query()->where('uuid', $uuid)->update(['status' => 'Rejected']);
                    return redirect()->back()->with('danger', 'You have rejected this DPI Assessment.');
                    return redirect()->back();
                }
                else {
                    return redirect()->back();
                }
                break;

            case 'approve':
                if ($dpiassessment->status == null) {
                    DPIAssessment::query()->where('uuid', $uuid)->update(['status' => 'Approved']);
                    return redirect()->back()->with('success', 'You have approved this DPI Assessment.');
                }
                else {
                    return redirect()->back();
                }
                break;
        }
    }
}
