<?php

namespace GDPR_Compliance_Assessment\Http\Controllers\Dashboard;

use GDPR_Compliance_Assessment\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }
}
