<?php

namespace GDPR_Compliance_Assessment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {

        if (Auth::check()) 
        {
            return redirect()->route('dashboard');
        }

        return view('home');
    }

    public function indexTermsOfUse()
    {
        return view('home.termsofuse');
    }

    public function indexPrivacyPolicy()
    {
        return view('home.privacypolicy');
    }
}
