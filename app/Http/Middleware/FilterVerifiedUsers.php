<?php

namespace GDPR_Compliance_Assessment\Http\Middleware;

use Closure;
use Auth;

class FilterVerifiedUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try
        {

          $user = Auth::user();

          if (!$user->verification_token == null) {
              Auth::logout();
              return redirect()->route('auth.signin')->with('warning', 'Your account has not yet been verified.');
          }

        }

        catch (\Exception $e)
        {
          return redirect()->route('auth.signin')->with('warning', 'Please sign-in to continue.');
        }

        return $next($request);
    }
}
