<?php

namespace GDPR_Compliance_Assessment\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use GDPR_Compliance_Assessment\Models\DPIAssessment;

class FilterDPIAssessmentOwners
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dpiassessment_uuid = $request->segments()[1];
        /*$dpiassessment = DPIAssessment::where('uuid', '=', $dpiassessment_uuid)->get();*/
        $dpiassessment = DPIAssessment::findOrFail($dpiassessment_uuid); 

        if ($dpiassessment->username !== Auth::user()->username) {
            abort(403, 'Unauthorized action.');
        }

        return $next($request);
    }
}
