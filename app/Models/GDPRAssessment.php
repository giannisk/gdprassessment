<?php

namespace GDPR_Compliance_Assessment\Models;

use Illuminate\Database\Eloquent\Model;

class GDPRAssessment extends Model
{
    protected $table = 'gdprassessments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'username',
        'first_name',
        'last_name',
        'organisation_name',
        'job_title',
        'organisation_role',
        'EU_data_subjects',
        'technical_organisational_measures',
        'records_processing_activities',
        'privacy_policy',
        'data_protection_design_default',
        'data_breach_procedure',
        'data_protection_officer',
        'list_subprocessors',
        'contract_data_controllers',
        'friendly_name',
        'description',
        'controller_name',
        'processor_name',
        'storage_method',
        'data_type',
        'estimated_size',
        'storage_period',
        'security_measures',
        'processing_principles',
        'data_subject_rights',
        'legal_justification',
        'selected_processing_activities',
        'comments',
        'status',
    ];

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Relates Processing Acitivites with the GDPR Assessment.
     *
     *
     */
    public function processingactivities()
    {
        return $this->belongsToMany('GDPR_Compliance_Assessment\Models\ProcessingActivity', 'gdprassessments_processingactivities', 
          'gdprassessment_uuid', 'processingactivity_uuid');
    }
}
