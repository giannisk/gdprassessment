<?php

namespace GDPR_Compliance_Assessment\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessingActivity extends Model
{
    protected $table = 'processingactivities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'username',
        'first_name',
        'last_name',
        'organisation_name',
        'job_title',
        'processingactivity_name',
        'controller_name',
        'processor_name',
        'processingactivity_description',
        'storage_method',
        'data_type',
        'legal_justification',
        'security_measures',
        'processing_principles',
        'data_subject_rights',
        'gdprassessment_uuid',
    ];

    protected $casts = [
        'uuid',
        'username',
        'first_name',
        'last_name',
        'organisation_name',
        'job_title',
        'processingactivity_name',
        'controller_name',
        'processor_name',
        'processingactivity_description',
        'storage_method',
        'data_type',
        'legal_justification',
        'security_measures',
        'processing_principles',
        'data_subject_rights',
        'gdprassessment_uuid',
    ];

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
}
