<?php

namespace GDPR_Compliance_Assessment\Models;

use Illuminate\Database\Eloquent\Model;

class DPIAssessment extends Model
{
    protected $table = 'dpiassessments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'username',
        'first_name',
        'last_name',
        'organisation_name',
        'job_title',
        'data_processing',
        'responsibilities',
        'relevant_standards',
        'data_involved',
        'data_life_cycle',
        'data_supporting_assets',
        'explicitness_and_legitimacy',
        'lawfulness',
        'data_minimisation',
        'data_accuracy',
        'storage_duration',
        'communication',
        'access_and_portability',
        'rectification_and_erasure',
        'restriction_and_object',
        'contract_governance',
        'international_data_transfer',
        'additional_information',
        'security_measures',
        'r1_impact_data_subjects',
        'r1_main_threats',
        'r1_risk_sources',
        'r1_risk_severity',
        'r1_risk_likelihood',
        'r2_impact_data_subjects',
        'r2_main_threats',
        'r2_risk_sources',
        'r2_risk_severity',
        'r2_risk_likelihood',
        'r3_impact_data_subjects',
        'r3_main_threats',
        'r3_risk_sources',
        'r3_risk_severity',
        'r3_risk_likelihood',
        'additonal_measures',
        'dpo_opinion',
        'status',
    ];

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
}
