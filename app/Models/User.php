<?php

namespace GDPR_Compliance_Assessment\Models;

use GDPR_Compliance_Assessment\Notifications\VerifyEmail;
use GDPR_Compliance_Assessment\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'username',
        'email',
        'password',
        'first_name',
        'last_name',
        'verification_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Returns true if the user is verified.
     *
     * @return bool
     */
    public function verified()
    {
        return $this->verification_token === null;
    }

    /**
     * Sends the verify e-mail notification.
     *
     * @return void
     */
    public function sendVerificationEmail()
    {
        return $this->notify(new VerifyEmail($this));
    }

    /**
     * Sends the password reset notification.
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        return $this->notify(new ResetPassword($token));
    }
}
