// Hide Div
hideDiv = function () {
  $("#for_processor").hide();
};

// Show Div
handleSelection = function () {
  hideDiv();
  switch ($(this).val()) {
    case 'My organisation processes data on behalf of another organisation.':
        $("#for_processor").show();
    break;
    case 'My organisation both determines the purposes and means of data processing and processes data.':
        $("#for_processor").show();
    break;
  }
};

$(document).ready(function() {
  $("#organisation_role").change(handleSelection);
  handleSelection.apply($("#organisation_role"));
});


