<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDpiassessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpiassessments', function (Blueprint $table) {
            $table->string('uuid')->primary();
            $table->string('username');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('organisation_name');
            $table->string('job_title');
            $table->string('data_processing');
            $table->string('responsibilities');
            $table->string('relevant_standards');
            $table->string('data_involved');
            $table->string('data_life_cycle');
            $table->string('data_supporting_assets');
            $table->string('explicitness_and_legitimacy');
            $table->string('lawfulness');
            $table->string('data_minimisation');
            $table->string('data_accuracy');
            $table->string('storage_duration');
            $table->string('communication');
            $table->string('access_and_portability');
            $table->string('rectification_and_erasure');
            $table->string('restriction_and_object');
            $table->string('contract_governance');
            $table->string('international_data_transfer');
            $table->string('additional_information');
            $table->string('security_measures');
            $table->string('r1_impact_data_subjects');
            $table->string('r1_main_threats');
            $table->string('r1_risk_sources');
            $table->string('r1_risk_severity');
            $table->string('r1_risk_likelihood');
            $table->string('r2_impact_data_subjects');
            $table->string('r2_main_threats');
            $table->string('r2_risk_sources');
            $table->string('r2_risk_severity');
            $table->string('r2_risk_likelihood');
            $table->string('r3_impact_data_subjects');
            $table->string('r3_main_threats');
            $table->string('r3_risk_sources');
            $table->string('r3_risk_severity');
            $table->string('r3_risk_likelihood');
            $table->string('additional_measures')->nullable();
            $table->string('dpo_opinion')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dpiassessments');
    }
}
