<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessingActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processingactivities', function (Blueprint $table) {
            $table->string('uuid')->primary();
            $table->string('username');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('organisation_name');
            $table->string('job_title');
            $table->string('processingactivity_name');
            $table->string('controller_name');
            $table->string('processor_name');
            $table->string('processingactivity_description');
            $table->string('storage_method');
            $table->string('data_type');
            $table->string('legal_justification');
            $table->string('security_measures');
            $table->string('processing_principles');
            $table->string('data_subject_rights');
            $table->string('gdprassessment_uuid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processingactivities');
    }
}
