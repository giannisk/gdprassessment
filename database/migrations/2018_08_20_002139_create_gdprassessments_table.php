<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGdprassessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdprassessments', function (Blueprint $table) {
            $table->string('uuid')->primary();
            $table->string('username');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('organisation_name');
            $table->string('job_title');
            $table->string('organisation_role');
            $table->string('EU_data_subjects');
            $table->string('technical_organisational_measures');
            $table->string('records_processing_activities');
            $table->string('privacy_policy');
            $table->string('data_protection_design_default');
            $table->string('data_breach_procedure');
            $table->string('data_protection_officer');
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdprassessments');
    }
}
