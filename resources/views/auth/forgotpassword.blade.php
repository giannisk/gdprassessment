<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Forgot Password</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="bg-dark">

    <!-- Forgot Password -->
    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header text-center pt-auto">
          <h4>Forgot your password?</h4>
          <p class="mb-0">Enter your e-mail address and we will send you instructions to reset your password.</p>
        </div>
        <div class="card-body">
@if (Session::has('danger'))
          <div class="alert alert-danger text-center">{{ Session::get('danger') }}</div>
@elseif (Session::has('warning'))
          <div class="alert alert-warning text-center">{{ Session::get('warning') }}</div>
@elseif (Session::has('success'))
          <div class="alert alert-success text-center">{{ Session::get('success') }}</div>
@endif
          <div class="text-center">
          </div>
          <form method="post" action="{{ route('auth.forgotpassword') }}">
            <div class="form-group">
              <label for="email">E-Mail Address</label>
              <input class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" name="email" id="email" type="email" placeholder="E-Mail Address" value="{{ Request::old('email') ?: '' }}">
@if ($errors->has('email'))
              <span class="help-block">{{ $errors->first('email') }}</span>
@endif
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Forgot Password</button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="{{ route('auth.signup') }}">Account Sign Up</a>
            <a class="d-block small" href="{{ route('auth.signin') }}">Already Have An Account?</a>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  </body>

</html>
