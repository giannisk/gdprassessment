<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Reset Password</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="bg-dark">

    <!-- Reset Password -->
    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header text-center pt-auto">
          <h4>Let's reset your password!</h4>
          <p class="mb-0">Enter your new password twice to reset.</p>
        </div>
        <div class="card-body">
@if (Session::has('danger'))
          <div class="alert alert-danger text-center">{{ Session::get('danger') }}</div>
@elseif (Session::has('warning'))
          <div class="alert alert-warning text-center">{{ Session::get('warning') }}</div>
@elseif (Session::has('success'))
          <div class="alert alert-success text-center">{{ Session::get('success') }}</div>
@endif
          <form method="post" action="{{ route('auth.resetpassword', ['token' => $token]) }}">
            <div class="form-group">
              <label for="email">E-Mail Address</label>
              <input class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" name="email" id="email" type="email" placeholder="Enter E-mail Address" value="{{ $email }}">
@if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
@endif
            </div>
            <div class="form-group">
              <label for="password">New Password</label>
              <input class="form-control {{ $errors->has('password') ? 'is-invalid' : ''}}" name="password" id="password" type="password" placeholder="New Password">
@if ($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
@endif
            </div>
            <div class="form-group">
              <label for="password_confirmation">Confirm New Password</label>
              <input class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : ''}}" name="password_confirmation" id="password_confirmation" type="password" placeholder="Confirm New Password">
@if ($errors->has('password_confirmation'))
                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
@endif
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            <input type="hidden" name="token" value="{{ $token }}">
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="{{ route('auth.signup') }}">Account Sign Up</a>
            <a class="d-block small" href="{{ route('auth.signin') }}">Already Have An Account?</a>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  </body>

</html>
