<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Sign Up</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header text-center pt-auto">
          <h4>A journey of a thousand miles...</h4>
          <p class="mb-0">Enter an actual e-mail address and your desired username and password to sign-up for an account. Leave the rest to us!</p>
        </div>
        <div class="card-body">
        @if (Session::has('danger'))
          <div class="alert alert-danger text-center">{{ Session::get('danger') }}</div>
        @elseif (Session::has('warning'))
          <div class="alert alert-warning text-center">{{ Session::get('warning') }}</div>
        @elseif (Session::has('success'))
          <div class="alert alert-success text-center">{{ Session::get('success') }}</div>
        @endif
          <form method="post" action="{{ route('auth.signup') }}">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <label for="first_name">First Name</label>
                  <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : ''}}" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ Request::old('first_name') ?: '' }}">
                  @if ($errors->has('first_name'))
                    <span class="help-block">{{ $errors->first('first_name') }}</span>
                  @endif
                </div>
                <div class="col-md-6">
                  <label for="last_name">Last Name</label>
                  <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : ''}}" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ Request::old('last_name') ?: '' }}">
                  @if ($errors->has('last_name'))
                    <span class="help-block">{{ $errors->first('last_name') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <label for="email">E-Mail Address</label>
                  <input class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" name="email" id="email" type="email" placeholder="E-Mail Address" value="{{ Request::old('email') ?: '' }}">
                  @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="col-md-6">
                  <label for="username">Username</label>
                  <input class="form-control {{ $errors->has('username') ? 'is-invalid' : ''}}" name="username" id="username" type="text" placeholder="Username" value="{{ Request::old('username') ?: '' }}">
                  @if ($errors->has('username'))
                    <span class="help-block">{{ $errors->first('username') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <label for="password">Password</label>
                  <input class="form-control {{ $errors->has('password') ? 'is-invalid' : ''}}" name="password" id="password" type="password" placeholder="Password" value="">
                  @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="col-md-6">
                  <label for="password_confirmation">Confirm Password</label>
                  <input class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : ''}}" name="password_confirmation" id="password_confirmation" type="password" placeholder="Confirm Password" value="">
                  @if ($errors->has('password_confirmation'))
                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="custom-control custom-checkbox my-1 mr-sm-2 pb-2">
              <input type="checkbox" class="custom-control-input {{ $errors->has('checkbox') ? 'is-invalid' : ''}}" name="checkbox" id="checkbox" value="1">
              <label class="custom-control-label" for="checkbox">I have read and accept the <a href="{{ route('home.termsofuse') }}">terms of use</a> and <a href="{{ route('home.privacypolicy') }}">privacy policy</a>.</label>
              @if ($errors->has('checkbox'))
                <span class="help-block">{{ $errors->first('checkbox') }}</span>
              @endif
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="{{ route('auth.signin') }}">Already Have An Account?</a>
            <a class="d-block small" href="{{ route('auth.forgotpassword') }}">Forgot Password?</a>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  </body>

</html>
