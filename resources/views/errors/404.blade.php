<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / 404</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header text-center pt-auto">
          <h4>Oops!</h4>
          <p class="mb-0">We couldn't find that page.</p>
        </div>
        <div class="card-body text-center">
          <span class="display-1 d-block">404</span>
          <p class="lead"><a href="javascript:history.back()">Return</a> to the previous page.</p>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  </body>

</html>
