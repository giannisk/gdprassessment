<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>Dashboard</h1>
            <hr>
          </div>
        </div>

        <!-- Cards-->
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-database"></i>
                </div>
                <div class="mr-5">Processing Activity</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="{{ route('dashboard.processingactivity') }}">
                <span class="float-left">Submit Processing Activity</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-balance-scale"></i>
                </div>
                <div class="mr-5">GDPR Assessment</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="{{ route('dashboard.gdprassessment') }}">
                <span class="float-left">Begin GDPR Assessment</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-info o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5">DPI Assessment</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="{{ route('dashboard.dpiassessment') }}">
                <span class="float-left">Begin DPI Assessment</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-copy"></i>
                </div>
                <div class="mr-5">Completed Assessments</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="{{ route('dashboard.completedassessments') }}">
                <span class="float-left">View Completed Assessments</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
        </div>

        <!-- Jumbotron -->
        <div class="row">
          <div class="col-12">
            <div class="jumbotron text-center">
              <h1>Hello {{{ isset(Auth::user()->first_name) ? Auth::user()->first_name : Auth::user()->username }}} {{{ isset(Auth::user()->last_name) ? Auth::user()->last_name : ''}}}!</h1>
              <p>{{ config('app.name') }} is an open-source web-based system that helps evaluate your organisation's alignment with the EU GDPR.</p>
              <p><a class="btn btn-primary" href="{{ route('dashboard.gdprassessment') }}" role="button"><i class="fas fa-fw fa-balance-scale"></i> Begin GDPR Assessment</a></p>
            </div>
          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>

      </div>
    </div>

  </body>

</html>
