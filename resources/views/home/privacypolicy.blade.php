<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.home.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Privacy Policy</title>

    @include('templates.partials.home.stylesheet')
    <link href="{{ asset('css/home_ext.css') }}" rel="stylesheet">

  </head>

  <body>

    @include('templates.partials.home.navigation')

    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <div class="col-lg-8">

          <h2 class="mt-4">Privacy Policy</h2><hr>
          <h4>General Information</h4>
          <p>Supporting privacy and safeguarding personal data are our top priorities. This Privacy Policy describes the types of data that we collect and process on your behalf. If you have further questions or expect additional information regarding our Privacy Policy, do not hesitate to write to us at <a href="mailto:info@gdprassessment.io">info@gdprassessment.io.</a></p>
          <h4>Personal Information We Collect</h4>
          <p>We collect and store the following information about you:</p>
          <ol>
            <li>First and Last Name: to offer you a personalised experience.</li>
            <li>E-Mail Address: to send you important e-mail messages such as password reminders.</li>
          </ol>
          <p>We follow a standard procedure of managing log files. The information collected by log files includes internet protocol (IP) addresses, and date and time stamps. These are not associated with any information that is personally identifiable. Log files are exclusively practiced for security purposes and are destroyed within one week.</p>
          <p>{{ config('app.name') }} GDPR Compliance Assessment retains your personal information only for as long as is necessary for the purposes established within this privacy policy.</p>
          <h4>Your Rights</h4>
          <p>If you are a resident of the European Union (EU), you are entitled to the following rights:</p>
          <ol>
            <li>Right to Access: you may obtain a copy of your personal information.</li>
            <li>Right of Rectification: you may modify your personal information.</li>
            <li>Right to Object: you may object the processing of your personal information.</li>
            <li>Right of Restriction: you may request that we limit the processing of your personal information, if applicable.</li>
            <li>Right to Data portability: you may transfer your personal information to another service.</li>
          </ol>
          <h4>Children's Information</h4>
          <p>{{ config('app.name') }} does not purposely collect any personal information from children under the age of 13. If you think that your child provided this kind of information, we strongly encourage you to <a href="mailto:info@gdprassessment.io">contact us</a> immediately and we will do our best efforts to promptly remove such information from our records.</p>
          <h4>Consent</h4>
          <p>By using our website, you consent to this Privacy Policy and agree to its terms. You may withdraw your consent at any time by <a href="mailto:info@gdprassessment.io">letting us know</a>.</p><hr>
          <p>Last Update: 16 December 2018</a>
        </div>

      </div>
    </div>

    <br>

    @include('templates.partials.home.calltoaction')

    @include('templates.partials.home.footer')

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/main.min.js') }}"></script>
    <script src="{{ asset('js/active.js') }}"></script>

  </body>

</html>
