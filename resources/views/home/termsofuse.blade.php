<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.home.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Terms of Use</title>

    @include('templates.partials.home.stylesheet')
    <link href="{{ asset('css/home_ext.css') }}" rel="stylesheet">

  </head>

  <body>

    @include('templates.partials.home.navigation')

    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <div class="col-lg-8">

          <h2 class="mt-4">Terms of Use</h2>
          <hr>

          <h4>1. General Information</h4>
          <p>Welcome, and thank you for your interest in {{ config('app.name') }} (“{{ config('app.name') }},” “we,” “our,” or “us”). Unless otherwise noted on a particular website or service, these master terms of use (“Master Terms”) apply to your use of all of the websites that we operate. These include <a href="{{ url('/') }}">{{ url('/') }}</a>, together with all other subdomains thereof, (collectively, the “Websites”). The Master Terms also apply to all products, information, and services provided through the Websites, including without limitation the GDPR Assessment and DPI Assessment (together with the Websites, the “Services”).</p>
          <p>Collectively, The Master Terms, form a binding legal agreement between you and us in relation to your use of the Services. Collectively, this legal agreement is referred to below as the “Terms.”</p>
          <p><em>Summary: These terms, create a contract between you and us. The contract governs your use of all services operated by us, unless a particular services indicates otherwise. These summaries of each section are not part of the contract, but are intended to help you understand its terms.</em></p>

          <h4>2. Agreement to the Terms</h4>
          <p>BY CLICKING “I ACCEPT” OR OTHERWISE ACCESSING OR USING ANY OF THE SERVICES, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD, AND AGREED TO BE BOUND BY THE TERMS. By clicking “I ACCEPT” or otherwise accessing or using any Services you also represent that you have the legal authority to accept the Terms on behalf of yourself and any party you represent in connection with your use of any Services. If you do not agree to the Terms, you are not authorized to use any Services. If you are an individual who is entering into these Terms on behalf of an entity, you represent and warrant that you have the power to bind that entity, and you hereby agree on that entity’s behalf to be bound by these Terms, with the terms “you,” and “your” applying to you, that entity, and other users accessing the Services on behalf of that entity.</p>
          <p><em>Summary: Please read these terms and only use our sites and services if you agree to them.</em></p>

          <h4>3. Changes to the Terms</h4>
          <p>From time to time, we may change, remove, or add to the Terms, and reserve the right to do so in its discretion. In that case, we will post updated Terms and indicate the date of revision. We will make reasonable efforts to post a prominent notice on the relevant Website(s) and notify account owners via email. All new and/or revised Terms t will take effect 30 days after the change is made and apply to your use of the Services from that date on. Your continued use of any Services after new and/or revised Terms are effective indicates that you have read, understood, and agreed to those Terms.
          <p><em>Summary: These terms may change. If the terms change, we will put a notice on the website and notify account owners. If you continue to use the sites after the changes are made, you agree to the changes.</em></p>

          <h4>4. No Legal Advice</h4>
          <p>{{ config('app.name') }} is not a law or consulting firm, does not provide legal advice, and is not a substitute for a law or consulting firm. Sending us an email or using any of the Services, including the GDPR Assessment and DPI Assessment, does not constitute legal advice or create an attorney-client relationship.</p>
          <p><em>Summary: Please consult your own attorney if you need legal advice.</em></p>

          <h4>5. Content Supplied by You</h4>
          <p>You represent, warrant, and agree that no Content posted or otherwise shared by you on or through any of the Services (“Your Content”), violates or infringes upon the rights of any third party, including copyright, trademark, privacy, publicity, or other personal or proprietary rights, breaches or conflicts with any obligation, such as a confidentiality obligation, or contains libelous, defamatory, or otherwise unlawful material. You retain any copyright that you may have in Your Content.</p>
          <p><em>Summary: We do not take any ownership of your content when you post it on our websites. You are responsible for any content you upload to our websites.</em></p>

          <h4>6. Registered Users</h4>
          <p>By registering for an account through any of the Services, you represent and warrant that you are the age of majority in your jurisdiction (typically age 18). Services offered to registered users are provided subject to these Master Terms, the Privacy Policy, and any Additional Terms specified on the relevant Website(s), all of which are hereby incorporated by reference into these Terms.</p>
          <p>Registration: You agree to (a) only provide accurate and current information about yourself (though use of an alias or nickname in lieu of your legal name is encouraged), (b) maintain the security of your passwords and identification, (c) promptly update the email address listed in connection with your account to keep it accurate so that we can contact you, and (d) be fully responsible for all uses of your account. You must not set up an account on behalf of another individual or entity unless you are authorized to do so.</p>
          <p><em>Summary: Please do not register for an account on our websites unless you are 18 years old. You are responsible for use of your account. Please do not set up an account for someone else unless you have permission to do so. </em></p>

          <h4>7. Prohibited Conduct</h4>
          <p>You agree not to engage in any of the following activities:</p>
          <ul>
            <li><h5>Violating Laws and Rights</h5><p>You may not (a) use any Service for any illegal purpose or in violation of any local, state, national, or international laws, (b) violate or encourage others to violate any right of or obligation to a third party, including by infringing, misappropriating, or violating intellectual property, confidentiality, or privacy rights.</p></li>
            <li><h5>Disruption</h5><p>You may not use the Services in any manner that could disable, overburden, damage, or impair the Services, or interfere with any other party’s use and enjoyment of the Services; including by (a) uploading or otherwise disseminating any virus, adware, spyware, worm or other malicious code, or (b) interfering with or disrupting any network, equipment, or server connected to or used to provide any of the Services, or violating any regulation, policy, or procedure of any network, equipment, or server.</p></li>
            <li><h5>Impersonation</h5><p>You may not impersonate another person or entity, or misrepresent your affiliation with a person or entity when using the Services.</p></li>
            <li><h5>Unauthorized access</h5><p>You may not use or attempt to use another’s account or personal information without authorization. You may not attempt to gain unauthorized access to the Services, or the computer systems or networks connected to the Services, through hacking password mining or any other means.</p></li>
          </ul>
          <p><em>Summary: Be yourself. Don’t break the law or be disruptive.</em></p>

          <h4>8. DISCLAIMER OF WARRANTIES</h4>
          <p>TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, CREATIVE COMMONS OFFERS THE SERVICES (INCLUDING ALL CONTENT AVAILABLE ON OR THROUGH THE SERVICES) AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE SERVICES, EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, INCLUDING WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. WE DO NOT WARRANT THAT THE FUNCTIONS OF THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, THAT CONTENT MADE AVAILABLE ON OR THROUGH THE SERVICES WILL BE ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT ANY SERVERS USED BY US ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. WE DO NOT WARRANT OR MAKE ANY REPRESENTATION REGARDING USE OF THE CONTENT AVAILABLE THROUGH THE SERVICES IN TERMS OF ACCURACY, RELIABILITY, OR OTHERWISE.</p>
          <p><em>Summary: We do not make any guarantees about the websites, services, or content available on the sites.</em></p>

          <h4>9. LIMITATION OF LIABILITY</h4>
          <p>TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL WE BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY INCIDENTAL, DIRECT, INDIRECT, PUNITIVE, ACTUAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, OR OTHER DAMAGES, INCLUDING WITHOUT LIMITATION, LOSS OF REVENUE OR INCOME, LOST PROFITS, PAIN AND SUFFERING, EMOTIONAL DISTRESS, COST OF SUBSTITUTE GOODS OR SERVICES, OR SIMILAR DAMAGES SUFFERED OR INCURRED BY YOU OR ANY THIRD PARTY THAT ARISE IN CONNECTION WITH THE SERVICES (OR THE TERMINATION THEREOF FOR ANY REASON), EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
          <p>TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, WE ARE NOT RESPONSIBLE OR LIABLE WHATSOEVER IN ANY MANNER FOR ANY CONTENT POSTED ON OR AVAILABLE THROUGH THE SERVICES (INCLUDING CLAIMS OF INFRINGEMENT RELATING TO THAT CONTENT), FOR YOUR USE OF THE SERVICES, OR FOR THE CONDUCT OF THIRD PARTIES ON OR THROUGH THE SERVICES.
          <p>Certain jurisdictions do not permit the exclusion of certain warranties or limitation of liability for incidental or consequential damages, which means that some of the above limitations may not apply to you. IN THESE JURISDICTIONS, THE FOREGOING EXCLUSIONS AND LIMITATIONS WILL BE ENFORCED TO THE GREATEST EXTENT PERMITTED BY APPLICABLE LAW.</p>
          <p><em>Summary: We are not responsible for the content on the websites, your use of our services, or for the conduct of others on our websites.</em></p>

          <h4>10. Privacy Policy</h4>
          <p>We are committed to responsibly handling the information and data we collect through our Services in compliance with our <a href="{{ route('home.privacypolicy') }}">Privacy Policy</a>, which is incorporated by reference into these Master Terms. Please review the <a href="{{ route('home.privacypolicy') }}">Privacy Policy</a> so you are aware of how we collect and use your personal information.</p>
          <p><em>Summary: Please read our <a href="{{ route('home.privacypolicy') }}">Privacy Policy</a>. It is part of these terms, too.</em></p>

          <h4>11. Termination</h4>
          <p>By us: We may modify, suspend, or terminate the operation of, or access to, all or any portion of the Services at any time for any reason. Additionally, your individual access to, and use of, the Services may be terminated by us at any time and for any reason.</p>
          <p>By you: If you wish to terminate this agreement, you may immediately stop accessing or using the Services at any time.</p>
          <p>Automatic upon breach: Your right to access and use the Services automatically upon your breach of any of the Terms. For the avoidance of doubt, termination of the Terms does not require you to remove or delete any reference to previously-applied tools from your own Content.</p>
          <p><em>Summary: If you violate these terms, you may no longer use our websites.</em></p>

          <h4>12. Miscellaneous Terms</h4>
          <p>No agency relationship: The parties agree that no joint venture, partnership, employment, or agency relationship exists between you and us as a result of the Terms or from your use of any of the Services.</p>
          <p><em>We are glad you use our sites, but this agreement does not mean we are partners.</em></p>

          <hr>
          <p>Last Update: 16 November 2018</a>

        </div>

      </div>
    </div>

    <br>

    @include('templates.partials.home.calltoaction')

    @include('templates.partials.home.footer')

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/main.min.js') }}"></script>
    <script src="{{ asset('js/active.js') }}"></script>

  </body>

</html>
