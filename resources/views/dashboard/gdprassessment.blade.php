<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / GDPR Assessment</title>

    @include('templates.partials.dashboard.stylesheet')
    <link href="{{ asset('css/gdprassessment.css') }}" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">GDPR Assessment</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>GDPR Assessment</h1>
            <hr>
          </div>
        </div>

        <!-- GDPR Assessment -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <form method="post" action="{{ route('dashboard.gdprassessment') }}">
              <!-- Essential Information -->
              <h5>Essential Information <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row">
                <div class="form-group col-md-3">
                  <label for="first_name">First Name</label>
                  <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : ''}}" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ Request::old('first_name') ?: Auth::user()->first_name }}">
                  @if ($errors->has('first_name'))
                    <span class="help-block">{{ $errors->first('first_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="last_name">Last Name</label>
                  <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : ''}}" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ Request::old('last_name') ?: Auth::user()->last_name }}">
                  @if ($errors->has('last_name'))
                    <span class="help-block">{{ $errors->first('last_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="organisation_name">Organisation Name</label>
                  <input class="form-control {{ $errors->has('organisation_name') ? 'is-invalid' : ''}}" name="organisation_name" id="organisation_name" type="text" placeholder="Organisation Name" value="{{ Request::old('organisation_name') }}">
                  @if ($errors->has('organisation_name'))
                    <span class="help-block">{{ $errors->first('organisation_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="job_title">Job Title</label>
                  <input class="form-control {{ $errors->has('job_title') ? 'is-invalid' : ''}}" name="job_title" id="job_title" type="text" placeholder="Job Title" value="{{ Request::old('job_title') }}">
                  @if ($errors->has('job_title'))
                    <span class="help-block">{{ $errors->first('job_title') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="organisation_role">Which of the following best describes the role of your organisation?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('organisation_role') ? 'is-invalid' : ''}}" name="organisation_role" id="organisation_role">
                    <option selected value="{{ Request::old('organisation_role') ?: '' }}">{{ Request::old('organisation_role') ?: 'Choose...' }}</option>
                    <option value="My organisation determines the purposes and means of data processing.">My organisation determines the purposes and means of data processing.</option>
                    <option value="My organisation processes data on behalf of another organisation.">My organisation processes data on behalf of another organisation.</option>
                    <option value="My organisation jointly determines the purposes and means of data processing with another organisation.">My organisation jointly determines the purposes and means of data processing with another organisation.</option>
                    <option value="My organisation both determines the purposes and means of data processing and processes data.">My organisation both determines the purposes and means of data processing and processes data.</option>
                  </select>
                  @if ($errors->has('organisation_role'))
                    <span class="help-block">{{ $errors->first('organisation_role') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="EU_data_subjects">Does your organisation involve data subjects in the EU?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('EU_data_subjects') ? 'is-invalid' : ''}}" name="EU_data_subjects" id="EU_data_subjects">
                    <option selected value="{{ Request::old('EU_data_subjects') ?: '' }}">{{ Request::old('EU_data_subjects') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation processes personal data relating to data subjects in the EU.">Yes, my organisation processes personal data relating to data subjects in the EU.</option>
                    <option value="No, my organisation does not process personal data of EU residents.">No, my organisation does not process personal data relating to data subjects in the EU.</option>
                    <option value="It is unknown whether my organisation processes personal data relating to data subjects in the EU.">It is unknown whether my organisation processes personal data relating to data subjects in the EU.</option>
                  </select>
                  @if ($errors->has('EU_data_subjects'))
                    <span class="help-block">{{ $errors->first('EU_data_subjects') }}</span>
                  @endif
                </div>
              </div>

              <!-- Organisational Requirements -->
              <br><h5>Organisational Requirements <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="technical_organisational_measures">Has your organisation implemented appropriate technical and organisational measures?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('technical_organisational_measures') ? 'is-invalid' : ''}}" name="technical_organisational_measures" id="technical_organisational_measures">
                    <option selected value="{{ Request::old('technical_organisational_measures') ?: '' }}">{{ Request::old('technical_organisational_measures') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation has implemented appropriate technical and organisational measures for the safeguarding of personal data.">Yes, my organisation has implemented appropriate technical and organisational measures for the safeguarding of personal data.</option>
                    <option value="No, my organisation has not implemented appropriate technical and organisational measures for the safeguarding of personal data.">No, my organisation has not implemented appropriate technical and organisational measures for the safeguarding of personal data.</option>
                    <option value="It is unknown whether my organisation has implemented appropriate technical and organisational measures for the safeguarding of personal data.">It is unknown whether my organisation has implemented appropriate technical and organisational measures for the safeguarding of personal data.</option>
                  </select>
                  @if ($errors->has('technical_organisational_measures'))
                    <span class="help-block">{{ $errors->first('technical_organisational_measures') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="records_processing_activities">Does your organisation maintain records of processing activities?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('records_processing_activities') ? 'is-invalid' : ''}}" name="records_processing_activities" id="records_processing_activities">
                    <option selected value="{{ Request::old('records_processing_activities') ?: '' }}">{{ Request::old('records_processing_activities') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation maintains records of processing activities.">Yes, my organisation maintains records of processing activities.</option>
                    <option value="No, my organisation does not maintain records of processing activities.">No, my organisation does not maintain records of processing activities.</option>
                    <option value="It is unknown whether my organisation maintains records of processing activities.">It is unknown whether my organisation maintains records of processing activities.</option>
                  </select>
                  @if ($errors->has('records_processing_activities'))
                    <span class="help-block">{{ $errors->first('records_processing_activities') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="privacy_policy">Has your organisation published an easy-to-understand privacy policy?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('privacy_policy') ? 'is-invalid' : ''}}" name="privacy_policy" id="privacy_policy">
                  <option selected value="{{ Request::old('privacy_policy') ?: '' }}">{{ Request::old('privacy_policy') ?: 'Choose...' }}</option>
                    <option value="Yes, there is a publicly available privacy policy that explains data processing activities and guides data subjects through exercising their rights.">Yes, there is a publicly available privacy policy that explains data processing activities and guides data subjects through exercising their rights.</option>
                    <option value="No, there isn't such a document published anywhere.">No, there isn't such a document published anywhere.</option>
                    <option value="It is unknown whether my organisation has published an easy-to-understand privacy policy.">It is unknown whether my organisation has published an easy-to-understand privacy policy.</option>
                  </select>
                  @if ($errors->has('privacy_policy'))
                    <span class="help-block">{{ $errors->first('privacy_policy') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="data_protection_design_default">Does your organisation embrace data protection by design and by default?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('data_protection_design_default') ? 'is-invalid' : ''}}" name="data_protection_design_default" id="data_protection_design_default">
                    <option selected value="{{ Request::old('data_protection_design_default') ?: '' }}">{{ Request::old('data_protection_design_default') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation embraces data protection by design and by default in accordance with Art. 25 GDPR.">Yes, my organisation embraces data protection by design and by default in accordance with Art. 25 GDPR.</option>
                    <option value="No, my organisation does not embrace data protection by design and by default.">No, my organisation does not embrace data protection by design and by default.</option>
                    <option value="It is unknown whether my organisation embraces data protection by design and by default.">It is unknown whether my organisation embraces data protection by design and by default.</option>
                  </select>
                  @if ($errors->has('data_protection_design_default'))
                    <span class="help-block">{{ $errors->first('data_protection_design_default') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="data_breach_procedure">Has your organisation established a procedure in the event of a data breach?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('data_breach_procedure') ? 'is-invalid' : ''}}" name="data_breach_procedure" id="data_breach_procedure">
                    <option selected value="{{ Request::old('data_breach_procedure') ?: '' }}">{{ Request::old('data_breach_procedure') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation has established a procedure in the event of a data breach.">Yes, my organisation has established a procedure in the event of a data breach.</option>
                    <option value="No, my organisation has not established a procedure in the event of a data breach.">No, my organisation has not established a procedure in the event of a data breach.</option>
                    <option value="It is unknown whether my organisation has established a procedure in the event of a data breach.">It is unknown whether my organisation has established a procedure in the event of a data breach.</option>
                  </select>
                  @if ($errors->has('data_breach_procedure'))
                    <span class="help-block">{{ $errors->first('data_breach_procedure') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-6">
                  <label class="my-1 mr-2" for="data_protection_officer">Has your organisation appointed a data protection officer (DPO)?</label>
                  <select class="custom-select my-1 mr-sm-2 {{ $errors->has('data_protection_officer') ? 'is-invalid' : ''}}" name="data_protection_officer" id="data_protection_officer">
                    <option selected value="{{ Request::old('data_protection_officer') ?: '' }}">{{ Request::old('data_protection_officer') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation has appointed a data protection officer.">Yes, my organisation has appointed a data protection officer.</option>
                    <option value="No, my organisation does not need to appoint a data protection officer.">No, my organisation does not need to appoint a data protection officer.</option>
                    <option value="No, my organisation has not appointed a data protection officer.">No, my organisation has not appointed a data protection officer.</option>
                    <option value="It is unknown whether my organisation has appointed a data protection officer.">It is unknown whether my organisation has already appointed or needs to appoint a data protection officer.</option>
                  </select>
                  @if ($errors->has('data_protection_officer'))
                    <span class="help-block">{{ $errors->first('data_protection_officer') }}</span>
                  @endif
                </div>
              </div>
              <div id="for_processor">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="list_subprocessors">Does your organisation list sub-processors within the privacy policy?</label>
                    <select class="custom-select my-1 mr-sm-2 {{ $errors->has('list_subprocessors') ? 'is-invalid' : ''}}" name="list_subprocessors" id="list_subprocessors">
                      <option selected value="{{ Request::old('list_subprocessors') ?: '' }}">{{ Request::old('list_subprocessors') ?: 'Choose...' }}</option>
                      <option value="Yes, the privacy policy includes all sub-processors and their respective involvement in data processing activities.">Yes, the privacy policy includes all sub-processors and their respective involvement in data processing activities.</option>
                      <option value="No, this question is not applicable since my organisation does not co-operate with any sub-processors.">No, this question is not applicable since my organisation does not co-operate with any sub-processors.</option>
                      <option value="No, the privacy policy does not include all sub-processors although my organisation co-operates with them in data processing.">No, the privacy policy does not include all sub-processors although my organisation co-operates with them in data processing.</option>
                      <option value="It is unknown whether my organisation co-operates with any sub-processors or includes the latter in the privacy policy.">It is unknown whether my organisation co-operates with any sub-processors or includes the latter in the privacy policy.</option>
                    </select>
                    @if ($errors->has('list_subprocessors'))
                      <span class="help-block">{{ $errors->first('list_subprocessors') }}</span>
                    @endif
                  </div>
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="contract_data_controllers">Has your organisation signed a contract with the respective data controller(s)?</label>
                    <select class="custom-select my-1 mr-sm-2 {{ $errors->has('contract_data_controllers') ? 'is-invalid' : ''}}" name="contract_data_controllers" id="contract_data_controllers">
                      <option selected value="{{ Request::old('contract_data_controllers') ?: '' }}">{{ Request::old('contract_data_controllers') ?: 'Choose...' }}</option>
                      <option value="Yes, my organisation has signed a contract with the respective data controller(s).">Yes, my organisation has signed a contract with the respective data controller(s).</option>
                      <option value="No, my organisation has not signed a contract with the respective data controller(s).">No, my organisation has not signed a contract with the respective data controller(s).</option>
                      <option value="It is unknown whether my organisation has signed a contract with the respective data controller(s).">It is unknown whether my organisation has signed a contract with the respective data controller(s).</option>
                    </select>
                    @if ($errors->has('contract_data_controllers'))
                      <span class="help-block">{{ $errors->first('contract_data_controllers') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <!-- Processing Requirements -->
              <br><h5>Processing Requirements <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="row">
@if(!count($processingactivities))
                <div class="col-lg-12">
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <p class="mb-auto">There seem to be no existing records of processing activities. Consider <a href="{{ route('dashboard.processingactivity') }}" class="alert-link">submitting processing activities</a> before conducting this assessment.</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                </div>
@else
                <div class="col-lg-12">
                  <table class="table table-striped text-center">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Processing Activity</th>
                        <th scope="col">Organisation Name</th>
                        <th scope="col">Date and Time</th>
                        <th scope="col">Select</th>
                      </tr>
                    </thead>
                    <tbody>
    @foreach($processingactivities as $processingactivity)
                      <tr>
                        <td class="align-middle">{{ $loop->iteration }}</td>
                        <td class="align-middle"><a href="{{ route('dashboard.view.processingactivity', [$processingactivity->uuid]) }}">{{$processingactivity->processingactivity_name}}</a></td>
                        <td class="align-middle">{{ $processingactivity->organisation_name }}</td>
                        <td class="align-middle">{{ $processingactivity->created_at }}</td>
                        <td class="align-middle"><input type="checkbox" name="selected_processing_activities[]" value="{{ $processingactivity->uuid }}"></td>
                      </tr>
  @endforeach
                    </tbody>
                  </table>
                </div>
@endif
              </div>
              <div class="form-group">
                <div class="row pt-3 pb-3">
                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-fw fa-balance-scale"></i> Submit GDPR Assessment</button>
                  </div>
                </div>
              </div>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/gdprassessment.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>

      </div>
    </div>

  </body>

</html>
