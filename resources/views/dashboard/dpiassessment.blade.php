<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / DPI Assessment</title>

    @include('templates.partials.dashboard.stylesheet')
    <link href="{{ asset('css/dpiassessment.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tagsinput.css') }}" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">DPI Assessment</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>DPI Assessment</h1>
            <hr>
          </div>
        </div>

        <!-- DPI Assessment -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <form method="post" action="{{ route('dashboard.dpiassessment') }}">
              <!-- Context -->
              <h5>Context <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row">
                <div class="form-group col-md-3">
                  <label for="first_name">First Name</label>
                  <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : ''}}" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ Request::old('first_name') ?: Auth::user()->first_name }}">
                  @if ($errors->has('first_name'))
                  <span class="help-block">{{ $errors->first('first_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="last_name">Last Name</label>
                  <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : ''}}" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ Request::old('last_name') ?: Auth::user()->last_name }}">
                  @if ($errors->has('last_name'))
                  <span class="help-block">{{ $errors->first('last_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="organisation_name">Organisation Name</label>
                  <input class="form-control {{ $errors->has('organisation_name') ? 'is-invalid' : ''}}" name="organisation_name" id="organisation_name" type="text" placeholder="Organisation Name" value="{{ Request::old('organisation_name') }}">
                  @if ($errors->has('organisation_name'))
                  <span class="help-block">{{ $errors->first('organisation_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="job_title">Job Title</label>
                  <input class="form-control {{ $errors->has('job_title') ? 'is-invalid' : ''}}" name="job_title" id="job_title" type="text" placeholder="Job Title" value="{{ Request::old('job_title') }}">
                  @if ($errors->has('job_title'))
                  <span class="help-block">{{ $errors->first('job_title') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row fix-margin">
                <div class="form-group col-md-4">
                  <div class="form-group">
                    <label for="data_processing">Data Processing</label>
                    <textarea class="form-control {{ $errors->has('data_processing') ? 'is-invalid' : ''}}" rows="3" name="data_processing" id="data_processing" placeholder="Name the data processing activity, include a short description, and explain its essential processing purposes.">{{ Request::old('data_processing') }}</textarea>
                    @if ($errors->has('data_processing'))
                    <span class="help-block">{{ $errors->first('data_processing') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-4">
                  <div class="form-group">
                    <label for="responsibilities">Responsibilities</label>
                    <textarea class="form-control {{ $errors->has('responsibilities') ? 'is-invalid' : ''}}" rows="3" name="responsibilities" id="responsibilities" placeholder="Specify any associated data controller(s) and data processor(s), as well as their respective responsibilities.">{{ Request::old('responsibilities') }}</textarea>
                    @if ($errors->has('responsibilities'))
                    <span class="help-block">{{ $errors->first('responsibilities') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-4">
                  <div class="form-group">
                    <label for="relevant_standards">Relevant Standards</label>
                    <textarea class="form-control {{ $errors->has('relevant_standards') ? 'is-invalid' : ''}}" rows="3" name="relevant_standards" id="relevant_standards" placeholder="Designate any relevant codes of conduct and certifications.">{{ Request::old('relevant_standards') }}</textarea>
                    @if ($errors->has('relevant_standards'))
                    <span class="help-block">{{ $errors->first('relevant_standards') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="form-row fix-margin">
                <div class="form-group col-md-4">
                  <div class="form-group">
                    <label for="data_involved">Data Involved</label>
                    <textarea class="form-control {{ $errors->has('data_involved') ? 'is-invalid' : ''}}" rows="3" name="data_involved" id="data_involved" placeholder="Describe what kind of data is collected and processed, define the respective storage periods, and specify which persons hold access.">{{ Request::old('data_involved') }}</textarea>
                    @if ($errors->has('data_involved'))
                    <span class="help-block">{{ $errors->first('data_involved') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-4">
                  <div class="form-group">
                    <label for="data_life_cycle">Data Life Cycle</label>
                    <textarea class="form-control {{ $errors->has('data_life_cycle') ? 'is-invalid' : ''}}" rows="3" name="data_life_cycle" id="data_life_cycle" placeholder="Explain the fundamental aspects of the process, and how data flows through information systems.">{{ Request::old('data_life_cycle') }}</textarea>
                    @if ($errors->has('data_life_cycle'))
                    <span class="help-block">{{ $errors->first('data_life_cycle') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-4">
                  <div class="form-group">
                    <label for="data_supporting_assets">Data Supporting Assets</label>
                    <textarea class="form-control {{ $errors->has('data_supporting_assets') ? 'is-invalid' : ''}}" rows="3" name="data_supporting_assets" id="data_supporting_assets" placeholder="Include any relevant data supporting assets, e.g., operating systems, applications and configurations.">{{ Request::old('data_supporting_assets') }}</textarea>
                    @if ($errors->has('data_supporting_assets'))
                    <span class="help-block">{{ $errors->first('data_supporting_assets') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <!-- Fundamental Principles -->
              <br><h5>Fundamental Principles <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row fix-margin">
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="explicitness_and_legitimacy">Explicitness and Legitimacy</label>
                    <textarea class="form-control {{ $errors->has('explicitness_and_legitimacy') ? 'is-invalid' : ''}}" rows="3" name="explicitness_and_legitimacy" id="explicitness_and_legitimacy" placeholder="Justify why the processing purposes are specified, explicit and legitimate.">{{ Request::old('explicitness_and_legitimacy') }}</textarea>
                    @if ($errors->has('explicitness_and_legitimacy'))
                    <span class="help-block">{{ $errors->first('explicitness_and_legitimacy') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="lawfulness">Lawfulness</label>
                    <textarea class="form-control {{ $errors->has('lawfulness') ? 'is-invalid' : ''}}" rows="3" name="lawfulness" id="lawfulness" placeholder="Provide a legal basis for the lawful processing of personal data.">{{ Request::old('lawfulness') }}</textarea>
                    @if ($errors->has('lawfulness'))
                    <span class="help-block">{{ $errors->first('lawfulness') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="data_minimisation">Data Minimisation</label>
                    <textarea class="form-control {{ $errors->has('data_minimisation') ? 'is-invalid' : ''}}" rows="3" name="data_minimisation" id="data_minimisation" placeholder="Describe how data are adequate, relevant and limited to what is necessary for the purposes for which they are processed.">{{ Request::old('data_minimisation') }}</textarea>
                    @if ($errors->has('data_minimisation'))
                    <span class="help-block">{{ $errors->first('data_minimisation') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="data_accuracy">Data Accuracy</label>
                    <textarea class="form-control {{ $errors->has('data_accuracy') ? 'is-invalid' : ''}}" rows="3" name="data_accuracy" id="data_accuracy" placeholder="Explain how data remain accurate and up-to-date.">{{ Request::old('data_accuracy') }}</textarea>
                    @if ($errors->has('data_accuracy'))
                    <span class="help-block">{{ $errors->first('data_accuracy') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="form-row fix-margin">
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="storage_duration">Storage Duration</label>
                    <textarea class="form-control {{ $errors->has('storage_duration') ? 'is-invalid' : ''}}" rows="3" name="storage_duration" id="storage_duration" placeholder="Determine the estimated storage duration(s) of the data involved.">{{ Request::old('storage_duration') }}</textarea>
                    @if ($errors->has('storage_duration'))
                    <span class="help-block">{{ $errors->first('storage_duration') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="communication">Communication</label>
                    <textarea class="form-control {{ $errors->has('communication') ? 'is-invalid' : ''}}" rows="3" name="communication" id="communication" placeholder="Specify what kind of information the data subjects receive and via which means of communication.">{{ Request::old('communication') }}</textarea>
                    @if ($errors->has('communication'))
                    <span class="help-block">{{ $errors->first('communication') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="access_and_portability">Access and Portability</label>
                    <textarea class="form-control {{ $errors->has('access_and_portability') ? 'is-invalid' : ''}}" rows="3" name="access_and_portability" id="access_and_portability" placeholder="Mention whether and how data subjects can exercise their right of access and their right to portability.">{{ Request::old('access_and_portability') }}</textarea>
                    @if ($errors->has('access_and_portability'))
                    <span class="help-block">{{ $errors->first('access_and_portability') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="rectification_and_erasure">Rectification and Erasure</label>
                    <textarea class="form-control {{ $errors->has('rectification_and_erasure') ? 'is-invalid' : ''}}" rows="3" name="rectification_and_erasure" id="rectification_and_erasure" placeholder="Mention whether and how data subjects can exercise their right to rectification and their right to erasure.">{{ Request::old('rectification_and_erasure') }}</textarea>
                    @if ($errors->has('rectification_and_erasure'))
                    <span class="help-block">{{ $errors->first('rectification_and_erasure') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="form-row fix-margin">
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="restriction_and_object">Restriction and Object</label>
                    <textarea class="form-control {{ $errors->has('restriction_and_object') ? 'is-invalid' : ''}}" rows="3" name="restriction_and_object" id="restriction_and_object" placeholder="Mention whether and how data subjects can exercise their right to restriction and their right to object.">{{ Request::old('restriction_and_object') }}</textarea>
                    @if ($errors->has('restriction_and_object'))
                    <span class="help-block">{{ $errors->first('restriction_and_object') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="contract_governance">Contract Governance</label>
                    <textarea class="form-control {{ $errors->has('contract_governance') ? 'is-invalid' : ''}}" rows="3" name="contract_governance" id="contract_governance" placeholder="Elaborate on the responsibilities of the processor(s) and the existence of relevant standards.">{{ Request::old('contract_governance') }}</textarea>
                    @if ($errors->has('contract_governance'))
                    <span class="help-block">{{ $errors->first('contract_governance') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="international_data_transfer">International Data Transfer</label>
                    <textarea class="form-control {{ $errors->has('international_data_transfer') ? 'is-invalid' : ''}}" rows="3" name="international_data_transfer" id="international_data_transfer" placeholder="If applicable, name the countries, and describe the corresponding data protection levels and provisions.">{{ Request::old('international_data_transfer') }}</textarea>
                    @if ($errors->has('international_data_transfer'))
                    <span class="help-block">{{ $errors->first('international_data_transfer') }}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group col-md-3">
                  <div class="form-group">
                    <label for="additional_information">Additional Information</label>
                    <textarea class="form-control {{ $errors->has('additional_information') ? 'is-invalid' : ''}}" rows="3" name="additional_information" id="additional_information" placeholder="Provide any additional information you deem important.">{{ Request::old('additional_information') }}</textarea>
                    @if ($errors->has('additional_information'))
                    <span class="help-block">{{ $errors->first('additional_information') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <!-- Risks -->
              <br><h5>Risks <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="security_measures">Security Measures</label>
                  <input data-role="tagsinput" class="form-control {{ $errors->has('security_measures') ? 'is-invalid' : ''}}" name="security_measures" id="security_measures" type="text" placeholder="Enter Security Measures" value="{{ Request::old('security_measures') }}">
                  @if ($errors->has('security_measures'))
                  <span class="help-block">{{ $errors->first('security_measures') }}</span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <br><h6 class="text-center">R1: Illegitimate Access to Data</h6><hr width="25%">
                  <div class="form-row fix-margin">
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r1_impact_data_subjects">Impact on Data Subjects</label>
                        <textarea class="form-control {{ $errors->has('r1_impact_data_subjects') ? 'is-invalid' : ''}}" rows="3" name="r1_impact_data_subjects" id="r1_impact_data_subjects" placeholder="Describe the potential impacts associated with illegitimate access to data.">{{ Request::old('r1_impact_data_subjects') }}</textarea>
                        @if ($errors->has('r1_impact_data_subjects'))
                        <span class="help-block">{{ $errors->first('r1_impact_data_subjects') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r1_main_threats">Main Threats</label>
                        <textarea class="form-control {{ $errors->has('r1_main_threats') ? 'is-invalid' : ''}}" rows="3" name="r1_main_threats" id="r1_main_threats" placeholder="Designate the main threats associated with illegitimate access to data.">{{ Request::old('r1_main_threats') }}</textarea>
                        @if ($errors->has('r1_main_threats'))
                        <span class="help-block">{{ $errors->first('r1_main_threats') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r1_risk_sources">Risk Sources</label>
                        <textarea class="form-control {{ $errors->has('r1_risk_sources') ? 'is-invalid' : ''}}" rows="3" name="r1_risk_sources" id="r1_risk_sources" placeholder="Determine the risk sources associated with illegitimate access to data.">{{ Request::old('r1_risk_sources') }}</textarea>
                        @if ($errors->has('r1_risk_sources'))
                        <span class="help-block">{{ $errors->first('r1_risk_sources') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label class="my-1 mr-2" for="r1_risk_severity">Risk Severity</label>
                      <select class="custom-select my-1 mr-sm-2 {{ $errors->has('r1_risk_severity') ? 'is-invalid' : ''}}" name="r1_risk_severity" id="r1_risk_severity">
                        <option selected value="{{ Request::old('r1_risk_severity') ?: "" }}">{{ Request::old('r1_risk_severity') ?: "Choose..." }}</option>
                        <option value="Undefined">Undefined</option>
                        <option value="Negligible">Negligible</option>
                        <option value="Limited">Limited</option>
                        <option value="Important">Important</option>
                        <option value="Maximum">Maximum</option>
                      </select>
                      @if ($errors->has('r1_risk_severity'))
                      <span class="help-block">{{ $errors->first('r1_risk_severity') }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label class="my-1 mr-2" for="r1_risk_likelihood">Risk Likelihood</label>
                      <select class="custom-select my-1 mr-sm-2 {{ $errors->has('r1_risk_likelihood') ? 'is-invalid' : ''}}" name="r1_risk_likelihood" id="r1_risk_likelihood">
                        <option selected value="{{ Request::old('r1_risk_likelihood') ?: "" }}">{{ Request::old('r1_risk_likelihood') ?: "Choose..." }}</option>
                        <option value="Undefined">Undefined</option>
                        <option value="Negligible">Negligible</option>
                        <option value="Limited">Limited</option>
                        <option value="Important">Important</option>
                        <option value="Maximum">Maximum</option>
                      </select>
                      @if ($errors->has('r1_risk_likelihood'))
                      <span class="help-block">{{ $errors->first('r1_risk_likelihood') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <br><h6 class="text-center">R2: Unwanted Modification of Data</h6><hr width="25%">
                  <div class="form-row fix-margin">
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r2_impact_data_subjects">Impact on Data Subjects</label>
                        <textarea class="form-control {{ $errors->has('r2_impact_data_subjects') ? 'is-invalid' : ''}}" rows="3" name="r2_impact_data_subjects" id="r2_impact_data_subjects" placeholder="Describe the potential impacts associated with unwanted modification of data.">{{ Request::old('r2_impact_data_subjects') }}</textarea>
                        @if ($errors->has('r2_impact_data_subjects'))
                        <span class="help-block">{{ $errors->first('r2_impact_data_subjects') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r2_main_threats">Main Threats</label>
                        <textarea class="form-control {{ $errors->has('r2_main_threats') ? 'is-invalid' : ''}}" rows="3" name="r2_main_threats" id="r2_main_threats" placeholder="Designate the main threats associated with unwanted modification of data.">{{ Request::old('r2_main_threats') }}</textarea>
                        @if ($errors->has('r2_main_threats'))
                        <span class="help-block">{{ $errors->first('r2_main_threats') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r2_risk_sources">Risk Sources</label>
                        <textarea class="form-control {{ $errors->has('r2_risk_sources') ? 'is-invalid' : ''}}" rows="3" name="r2_risk_sources" id="r2_risk_sources" placeholder="Determine the risk sources associated with unwanted modification of data.">{{ Request::old('r2_risk_sources') }}</textarea>
                        @if ($errors->has('r2_risk_sources'))
                        <span class="help-block">{{ $errors->first('r2_risk_sources') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label class="my-1 mr-2" for="r2_risk_severity">Risk Severity</label>
                      <select class="custom-select my-1 mr-sm-2 {{ $errors->has('r2_risk_severity') ? 'is-invalid' : ''}}" name="r2_risk_severity" id="r2_risk_severity">
                        <option selected value="{{ Request::old('r2_risk_severity') ?: "" }}">{{ Request::old('r2_risk_severity') ?: "Choose..." }}</option>
                        <option value="Undefined">Undefined</option>
                        <option value="Negligible">Negligible</option>
                        <option value="Limited">Limited</option>
                        <option value="Important">Important</option>
                        <option value="Maximum">Maximum</option>
                      </select>
                      @if ($errors->has('r2_risk_severity'))
                      <span class="help-block">{{ $errors->first('r2_risk_severity') }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label class="my-1 mr-2" for="r2_risk_likelihood">Risk Likelihood</label>
                      <select class="custom-select my-1 mr-sm-2 {{ $errors->has('r2_risk_likelihood') ? 'is-invalid' : ''}}" name="r2_risk_likelihood" id="r2_risk_likelihood">
                        <option ng-model="2.RiskLikelihood" selected value="{{ Request::old('r2_risk_likelihood') ?: "" }}">{{ Request::old('r2_risk_likelihood') ?: "Choose..." }}</option>
                        <option value="Undefined">Undefined</option>
                        <option value="Negligible">Negligible</option>
                        <option value="Limited">Limited</option>
                        <option value="Important">Important</option>
                        <option value="Maximum">Maximum</option>
                      </select>
                      @if ($errors->has('r2_risk_likelihood'))
                      <span class="help-block">{{ $errors->first('r2_risk_likelihood') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <br><h6 class="text-center">R3: Data Dissappearance</h6><hr width="25%">
                  <div class="form-row fix-margin">
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r3_impact_data_subjects">Impact on Data Subjects</label>
                        <textarea class="form-control {{ $errors->has('r3_impact_data_subjects') ? 'is-invalid' : ''}}" rows="3" name="r3_impact_data_subjects" id="r3_impact_data_subjects" placeholder="Describe the potential impacts associated with data dissappearance.">{{ Request::old('r3_impact_data_subjects') }}</textarea>
                        @if ($errors->has('r3_impact_data_subjects'))
                        <span class="help-block">{{ $errors->first('r3_impact_data_subjects') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r3_main_threats">Main Threats</label>
                        <textarea class="form-control {{ $errors->has('r3_main_threats') ? 'is-invalid' : ''}}" rows="3" name="r3_main_threats" id="r3_main_threats" placeholder="Designate the main threats associated with data dissappearance.">{{ Request::old('r3_main_threats') }}</textarea>
                        @if ($errors->has('r3_main_threats'))
                        <span class="help-block">{{ $errors->first('r3_main_threats') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group col-md-12">
                      <div class="form-group">
                        <label for="r3_risk_sources">Risk Sources</label>
                        <textarea class="form-control {{ $errors->has('r3_risk_sources') ? 'is-invalid' : ''}}" rows="3" name="r3_risk_sources" id="r3_risk_sources" placeholder="Determine the risk sources associated with data dissappearance.">{{ Request::old('r3_risk_sources') }}</textarea>
                        @if ($errors->has('r3_risk_sources'))
                        <span class="help-block">{{ $errors->first('r3_risk_sources') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label class="my-1 mr-2" for="r3_risk_severity">Risk Severity</label>
                      <select class="custom-select my-1 mr-sm-2 {{ $errors->has('r3_risk_severity') ? 'is-invalid' : ''}}" name="r3_risk_severity" id="r3_risk_severity">
                        <option selected value="{{ Request::old('r3_risk_severity') ?: "" }}">{{ Request::old('r3_risk_severity') ?: "Choose..." }}</option>
                        <option value="Undefined">Undefined</option>
                        <option value="Negligible">Negligible</option>
                        <option value="Limited">Limited</option>
                        <option value="Important">Important</option>
                        <option value="Maximum">Maximum</option>
                      </select>
                      @if ($errors->has('r3_risk_severity'))
                      <span class="help-block">{{ $errors->first('r3_risk_severity') }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label class="my-1 mr-2" for="r3_risk_likelihood">Risk Likelihood</label>
                      <select class="custom-select my-1 mr-sm-2 {{ $errors->has('r3_risk_likelihood') ? 'is-invalid' : ''}}" name="r3_risk_likelihood" id="r3_risk_likelihood">
                        <option selected value="{{ Request::old('r3_risk_likelihood') ?: "" }}">{{ Request::old('r3_risk_likelihood') ?: "Choose..." }}</option>
                        <option value="Undefined">Undefined</option>
                        <option value="Negligible">Negligible</option>
                        <option value="Limited">Limited</option>
                        <option value="Important">Important</option>
                        <option value="Maximum">Maximum</option>
                      </select>
                      @if ($errors->has('r3_risk_likelihood'))
                      <span class="help-block">{{ $errors->first('r3_risk_likelihood') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <!-- Validation -->
              <br><h5>Validation <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="row">
                <div class="col-12">
                  <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <p class="mb-auto">Please submit this DPI Assessment before proceeding with its validation.</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="form-group pt-3 pb-3">
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <button type="submit" type="button" class="btn btn-primary"><i class="fas fa-fw fa-list"></i> Submit DPI Assessment</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

          </div>

        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>
        <script src="{{ asset('js/tagsinput.js') }}"></script>

      </div>
    </div>

  </body>

</html>
