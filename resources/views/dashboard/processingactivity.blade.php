<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / Processing Activity</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Processing Activity</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>Processing Activity</h1>
            <hr>
          </div>
        </div>

        <!-- Processing Activity -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form method="post" action="{{ route('dashboard.processingactivity') }}">
              <!-- Essential Information -->
              <h5>Essential Information <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row">
                <div class="form-group col-md-3">
                  <label for="first_name">First Name</label>
                  <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : ''}}" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ Request::old('first_name') ?: Auth::user()->first_name }}">
                  @if ($errors->has('first_name'))
                    <span class="help-block">{{ $errors->first('first_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="last_name">Last Name</label>
                  <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : ''}}" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ Request::old('last_name') ?: Auth::user()->last_name }}">
                  @if ($errors->has('last_name'))
                    <span class="help-block">{{ $errors->first('last_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="organisation_name">Organisation Name</label>
                  <input class="form-control {{ $errors->has('organisation_name') ? 'is-invalid' : ''}}" name="organisation_name" id="organisation_name" type="text" placeholder="Organisation Name" value="{{ Request::old('organisation_name') ?: '' }}">
                  @if ($errors->has('organisation_name'))
                    <span class="help-block">{{ $errors->first('organisation_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-3">
                  <label for="job_title">Job Title</label>
                  <input class="form-control {{ $errors->has('job_title') ? 'is-invalid' : ''}}" name="job_title" id="job_title" type="text" placeholder="Job Title" value="{{ Request::old('job_title') ?: '' }}">
                  @if ($errors->has('job_title'))
                    <span class="help-block">{{ $errors->first('job_title') }}</span>
                  @endif
                </div>
              </div>
              <!-- Summary -->
              <br><h5>Summary <a href="#"> <i class="fa fa-question-circle fa-xs"></i></a></h5><hr>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="processingactivity_name">Processing Activity Name</label>
                  <input class="form-control {{ $errors->has('processingactivity_name') ? 'is-invalid' : ''}}" name="processingactivity_name" id="processingactivity_name" placeholder="Processing Activity Name" value="{{ Request::old('processingactivity_name') ?: '' }}">
                  @if ($errors->has('processingactivity_name'))
                    <span class="help-block">{{ $errors->first('processingactivity_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="controller_name">Controller Name</label>
                  <input class="form-control {{ $errors->has('controller_name') ? 'is-invalid' : ''}}" name="controller_name" id="controller_name" placeholder="Controller Name" value="{{ Request::old('controller_name') ?: '' }}">
                  @if ($errors->has('controller_name'))
                    <span class="help-block">{{ $errors->first('controller_name') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="processor_name">Processor Name</label>
                  <input class="form-control {{ $errors->has('processor_name') ? 'is-invalid' : ''}}" name="processor_name" id="processor_name" placeholder="Processor Name" value="{{ Request::old('processor_name') ?: '' }}">
                  @if ($errors->has('processor_name'))
                    <span class="help-block">{{ $errors->first('processor_name') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row mb-3">
                <div class="col-md-12">
                  <label for="processingactivity_description">Processing Activity Description</label>
                  <textarea class="form-control {{ $errors->has('processingactivity_description') ? 'is-invalid' : ''}}" name="processingactivity_description" id="processingactivity_description" placeholder="Explain the purposes of the processing, and provide a brief description of the implemented technical and organisational measures. If your organisation transfers personal data to recipients in third countries or international organisations, please elaborate further." rows="3">{{ Request::old('processingactivity_description') ?: '' }}</textarea>
                  @if ($errors->has('processingactivity_description'))
                    <span class="help-block">{{ $errors->first('processingactivity_description') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="storage_method">Storage Method</label>
                  <select class="custom-select {{ $errors->has('storage_method') ? 'is-invalid' : ''}}" name="storage_method" id="storage_method">
                    <option selected value="{{ Request::old('storage_method') ?: '' }}">{{ Request::old('storage_method') ?: 'Choose...' }}</option>
                    <option value="Digital">Digital</option>
                    <option value="Physical">Physical</option>
                    <option value="Digital and Physical">Digital and Physical</option>
                  </select>
                  @if ($errors->has('storage_method'))
                    <span class="help-block">{{ $errors->first('storage_method') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="type">Data Type</label>
                  <select class="custom-select {{ $errors->has('data_type') ? 'is-invalid' : ''}}" name="data_type" id="data_type">
                    <option selected value="{{ Request::old('data_type') ?: '' }}">{{ Request::old('data_type') ?: 'Choose...' }}</option>
                    <option value="Personal Data">Personal Data</option>
                    <option value="Special Categories of Personal Data">Special Categories of Personal Data</option>
                  </select>
                  @if ($errors->has('data_type'))
                    <span class="help-block">{{ $errors->first('data_type') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="legal_justification">Legal Justification</label>
                  <select class="custom-select {{ $errors->has('legal_justification') ? 'is-invalid' : ''}}" name="legal_justification" id="legal_justification">
                    <option selected value="{{ Request::old('data_subject_rights') ?: '' }}">{{ Request::old('data_subject_rights') ?: '' }}Choose...</option>
                    <option value="Consent">Consent</option>
                    <option value="Contract">Contract</option>
                    <option value="Legal Obligation">Legal Obligation</option>
                    <option value="Vital Interests">Vital Interests</option>
                    <option value="Public Task">Public Task</option>
                    <option value="Legitimate Interests">Legitimate Interests</option>
                    <option value="None">None</option>
                  </select>
                  @if ($errors->has('legal_justification'))
                    <span class="help-block">{{ $errors->first('legal_justification') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="security_measures">Security Measures</label>
                  <select class="custom-select {{ $errors->has('security_measures') ? 'is-invalid' : ''}}" name="security_measures" id="security_measures">
                    <option selected value="{{ Request::old('security_measures') ?: '' }}">{{ Request::old('security_measures') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation has implemented all appropriate technical and organisational measures.">Yes, my organisation has implemented all appropriate technical and organisational measures.</option>
                    <option value="Yes, my organisation has implemented some appropriate technical and organisational measures.">Yes, my organisation has implemented some appropriate technical and organisational measures.</option>
                    <option value="No, my organisation has not implemented any technical and organisational measures.">No, my organisation has not implemented any technical and organisational measures.</option>
                    <option value="It is unknown whether my organisation has implemented any appropriate technical and organisational measures.">It is unknown whether my organisation has implemented any appropriate technical and organisational measures.</option>
                  </select>
                  @if ($errors->has('security_measures'))
                    <span class="help-block">{{ $errors->first('security_measures') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="processing_principles">Processing Principles</label>
                  <select class="custom-select {{ $errors->has('processing_principles') ? 'is-invalid' : ''}}" name="processing_principles" id="processing_principles">
                    <option selected value="{{ Request::old('processing_principles') ?: '' }}">{{ Request::old('processing_principles') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation upholds all six processing principles.">Yes, my organisation upholds all six processing principles.</option>
                    <option value="Yes, my organisation upholds some processing principles.">Yes, my organisation upholds some processing principles.</option>
                    <option value="No, my organisation does not uphold any processing principle.">No, my organisation does not uphold any processing principle.</option>
                    <option value="It is unknown whether my organisation upholds any processing principle.">It is unknown whether my organisation upholds any processing principle.</option>
                  </select>
                  @if ($errors->has('processing_principles'))
                    <span class="help-block">{{ $errors->first('processing_principles') }}</span>
                  @endif
                </div>
                <div class="form-group col-md-4">
                  <label for="data_subject_rights">Data Subject Rights</label>
                  <select class="custom-select {{ $errors->has('data_subject_rights') ? 'is-invalid' : ''}}" name="data_subject_rights" id="data_subject_rights">
                    <option selected value="{{ Request::old('data_subject_rights') ?: '' }}">{{ Request::old('data_subject_rights') ?: 'Choose...' }}</option>
                    <option value="Yes, my organisation helps data subjects exercise all of their rights wherever applicable.">Yes, my organisation helps data subjects exercise all of their rights wherever applicable.</option>
                    <option value="Yes, my organisation helps data subjects exercise some of their rights.">Yes, my organisation helps data subjects exercise some of their rights.</option>
                    <option value="No, my organisation does not help data subjects exercise their rights.">No, my organisation does not help data subjects exercise their rights.</option>
                    <option value="It is unknown wherever my organisation helps data subjects exercise their rights">It is unknown wherever my organisation helps data subjects exercise their rights.</option>
                  </select>
                  @if ($errors->has('data_subject_rights'))
                    <span class="help-block">{{ $errors->first('data_subject_rights') }}</span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <div class="row pt-3 pb-3">
                  <div class="col-md-12 text-center">
                    <button type="submit" type="button" class="btn btn-primary"><i class="fas fa-fw fa-database"></i> Submit Processing Activity</button>
                  </div>
                </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
              </div>
            </form>

          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>

      </div>
    </div>

  </body>

</html>
