<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / DPI Assessment / View DPI Assessment</title>

    @include('templates.partials.dashboard.stylesheet')
    <link href="{{ asset('css/dpiassessment.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('css/angular-risk-matrix.css') }}" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard.dpiassessment') }}">DPI Assessment</a>
          </li>
          <li class="breadcrumb-item active">View DPI Assessment</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>DPI Assessment</h1>
            <hr>
          </div>
        </div>

        <!-- DPI Assessment -->
        <div class="row">
          <div class="col-12">

@if (Session::has('danger'))
            <div class="alert alert-danger text-center">{{ Session::get('danger') }}</div>
@elseif (Session::has('warning'))
            <div class="alert alert-warning text-center">{{ Session::get('warning') }}</div>
@elseif (Session::has('success'))
            <div class="alert alert-success text-center">{{ Session::get('success') }}</div>
@endif

            <!-- Assessment Result -->
            @if ($dpiassessment->status == "Approved")
            <div class="card text-white bg-success mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-check-circle"></i> Approved</h1>
                <p class="card-text lead">{{ config('app.name') }} does not currently offer automated evaluations of DPI Assessments. Please seek professional consulting if needed.</p>
              </div>
            </div>
            @elseif ($dpiassessment->status == "Rejected")
            <div class="card text-white bg-danger mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-times-circle"></i> Rejected</h1>
                <p class="card-text lead">{{ config('app.name') }} does not currently offer automated evaluations of DPI Assessments. Please seek professional consulting if needed.</p>
              </div>
            </div>
            @else
            <div class="card text-white bg-info mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-question-circle"></i> Pending Validation</h1>
                <p class="card-text lead">{{ config('app.name') }} does not currently offer automated evaluations of DPI Assessments. Please seek professional consulting if needed.</p>
              </div>
            </div>
            @endif

            <!-- Context -->
            <h5>Context</h5><hr>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="first_name">First Name</label>
                <input class="form-control" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ $dpiassessment->first_name }}">
              </div>
              <div class="form-group col-md-3">
                <label for="last_name">Last Name</label>
                <input class="form-control" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ $dpiassessment->last_name }}">
              </div>
              <div class="form-group col-md-3">
                <label for="organisation_name">Organisation Name</label>
                <input class="form-control" name="organisation_name" id="organisation_name" type="text" placeholder="Organisation Name" value="{{ Request::old('organisation_name') ?: $dpiassessment->organisation_name }}">
              </div>
              <div class="form-group col-md-3">
                <label for="job_title">Job Title</label>
                <input class="form-control" name="job_title" id="job_title" type="text" placeholder="Job Title" value="{{ $dpiassessment->job_title }}">
              </div>
            </div>
            <div class="form-row fix-margin">
              <div class="form-group col-md-4">
                <div class="form-group">
                  <label for="data_processing">Data Processing</label>
                  <textarea class="form-control" rows="3" name="data_processing" id="data_processing" placeholder="Friendly Name&#10;Short Description&#10;Data Processing Purposes">{{ $dpiassessment->data_processing }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-4">
                <div class="form-group">
                  <label for="responsibilities">Responsibilities</label>
                  <textarea class="form-control" rows="3" name="responsibilities" id="responsibilities" placeholder="Data Controller&#10;Data Processor&#10;Joint Controller">{{ $dpiassessment->responsibilities }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-4">
                <div class="form-group">
                  <label for="relevant_standards">Relevant Standards</label>
                  <textarea class="form-control" rows="3" name="relevant_standards" id="relevant_standards" placeholder="Codes of Conduct&#10;Certifications">{{ $dpiassessment->relevant_standards }}</textarea>
                </div>
              </div>
            </div>
            <div class="form-row fix-margin">
              <div class="form-group col-md-4">
                <div class="form-group">
                  <label for="data_involved">Data Involved</label>
                  <textarea class="form-control" rows="3" name="data_involved" id="data_involved" placeholder="Data Collected and Processed&#10;Storage Durations&#10;Recipients and Persons with Access">{{ $dpiassessment->data_involved }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-4">
                <div class="form-group">
                  <label for="data_life_cycle">Data Life Cycle</label>
                  <textarea class="form-control" rows="3" name="data_life_cycle" id="data_life_cycle" placeholder="Process Description&#10;Data Flow Description">{{ $dpiassessment->data_life_cycle }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-4">
                <div class="form-group">
                  <label for="data_supporting_assets">Data Supporting Assets</label>
                  <textarea class="form-control" rows="3" name="data_supporting_assets" id="data_supporting_assets" placeholder="Operating Systems&#10;Applications&#10;Configurations">{{ $dpiassessment->data_supporting_assets }}</textarea>
                </div>
              </div>
            </div>

            <!-- Fundamental Principles -->
            <br><h5>Fundamental Principles</h5><hr>
            <div class="form-row fix-margin">
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="explicitness_and_legitimacy">Explicitness and Legitimacy</label>
                  <textarea class="form-control" rows="3" name="explicitness_and_legitimacy" id="explicitness_and_legitimacy" placeholder="Processing Purpose Specification&#10;Processing Purpose Explicitness&#10;Processing Purpose Legitimacy">{{ $dpiassessment->explicitness_and_legitimacy }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="lawfulness">Lawfulness</label>
                  <textarea class="form-control" rows="3" name="lawfulness" id="lawfulness" placeholder="Legal Basis Description">{{ $dpiassessment->lawfulness }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="data_minimisation">Data Minimisation</label>
                  <textarea class="form-control" rows="3" name="data_minimisation" id="data_minimisation" placeholder="Data Minimisation Description">{{ $dpiassessment->data_minimisation }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="data_accuracy">Data Accuracy</label>
                  <textarea class="form-control" rows="3" name="data_accuracy" id="data_accuracy" placeholder="Data Accuracy Description">{{ $dpiassessment->data_accuracy }}</textarea>
                </div>
              </div>
            </div>
            <div class="form-row fix-margin">
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="storage_duration">Storage Duration</label>
                  <textarea class="form-control" rows="3" name="storage_duration" id="storage_duration" placeholder="Storage Duration Description">{{ $dpiassessment->storage_duration }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="communication">Communication</label>
                  <textarea class="form-control" rows="3" name="communication" id="communication" placeholder="Information Shared w/ Data Subjects&#10;Communication Means">{{ $dpiassessment->communication }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="access_and_portability">Access and Portability</label>
                  <textarea class="form-control" rows="3" name="access_and_portability" id="access_and_portability" placeholder="Right to Access Mechanisms&#10;Right to Portability Mechanisms">{{ $dpiassessment->access_and_portability }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="rectification_and_erasure">Rectification and Erasure</label>
                  <textarea class="form-control" rows="3" name="rectification_and_erasure" id="rectification_and_erasure" placeholder="Right to Rectification Mechanisms&#10;Right to Erasure Mechanisms">{{ $dpiassessment->rectification_and_erasure }}</textarea>
                </div>
              </div>
            </div>
            <div class="form-row fix-margin">
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="restriction_and_object">Restriction and Object</label>
                  <textarea class="form-control" rows="3" name="restriction_and_object" id="restriction_and_object" placeholder="Right to Restriction Mechanisms&#10;Right to Object Mechanisms">{{ $dpiassessment->restriction_and_object }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="contract_governance">Contract Governance</label>
                  <textarea class="form-control" rows="3" name="contract_governance" id="contract_governance" placeholder="Data Processor Responsibilities&#10;Relevant Contacts, CoCs and Certifications">{{ $dpiassessment->contract_governance }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="international_data_transfer">International Data Transfer</label>
                  <textarea class="form-control" rows="3" name="international_data_transfer" id="international_data_transfer" placeholder="Country Specification&#10;Data Protection Level&#10;Data Transfer Provisions">{{ $dpiassessment->international_data_transfer }}</textarea>
                </div>
              </div>
              <div class="form-group col-md-3">
                <div class="form-group">
                  <label for="additional_information">Additional Information</label>
                  <textarea class="form-control" rows="3" name="additional_information" id="additional_information" placeholder="Additional Information">{{ $dpiassessment->additional_information }}</textarea>
                </div>
              </div>
            </div>

            <!-- Risks -->
            <br><h5>Risks</h5><hr>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="security_measures">Security Measures</label>
                <input data-role="tagsinput" class="form-control" name="security_measures" id="security_measures" type="text" value="{{ $dpiassessment->security_measures }}">
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <br><h6 class="text-center">R1: Illegitimate Access to Data</h6><hr width="25%">
                <div class="form-row fix-margin">
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r1_impact_data_subjects">Impact on Data Subjects</label>
                      <textarea class="form-control" rows="3" name="r1_impact_data_subjects" id="r1_impact_data_subjects" placeholder="Potential Impact">{{ $dpiassessment->r1_impact_data_subjects }}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r1_main_threats">Main Threats</label>
                      <textarea class="form-control" rows="3" name="r1_main_threats" id="r1_main_threats" placeholder="Main Threats">{{ $dpiassessment->r1_main_threats }}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r1_risk_sources">Risk Sources</label>
                      <textarea class="form-control" rows="3" name="r1_risk_sources" id="r1_risk_sources" placeholder="Risk Sources">{{ $dpiassessment->data_involved }}</textarea>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="r1_risk_severity">Risk Severity</label>
                    <select class="custom-select my-1 mr-sm-2" name="r1_risk_severity" id="r1_risk_severity">
                      <option selected value="">{{ $dpiassessment->r1_risk_likelihood }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="r1_risk_likelihood">Risk Likelihood</label>
                    <select class="custom-select my-1 mr-sm-2" name="r1_risk_likelihood" id="r1_risk_likelihood">
                      <option selected value="">{{ $dpiassessment->r1_risk_likelihood }}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <br><h6 class="text-center">R2: Unwanted Modification of Data</h6><hr width="25%">
                <div class="form-row fix-margin">
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r2_impact_data_subjects">Impact on Data Subjects</label>
                      <textarea class="form-control" rows="3" name="r2_impact_data_subjects" id="r2_impact_data_subjects" placeholder="Potential Impact">{{ $dpiassessment->r2_impact_data_subjects }}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r2_main_threats">Main Threats</label>
                      <textarea class="form-control" rows="3" name="r2_main_threats" id="r2_main_threats" placeholder="Main Threats">{{ $dpiassessment->r2_main_threats }}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r2_risk_sources">Risk Sources</label>
                      <textarea class="form-control" rows="3" name="r2_risk_sources" id="r2_risk_sources" placeholder="Risk Sources">{{ $dpiassessment->r2_risk_sources }}</textarea>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="r2_risk_severity">Risk Severity</label>
                    <select class="custom-select my-1 mr-sm-2" name="r2_risk_severity" id="r2_risk_severity">
                      <option selected value="">{{ $dpiassessment->r2_risk_severity }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="r2_risk_likelihood">Risk Likelihood</label>
                    <select class="custom-select my-1 mr-sm-2" name="r2_risk_likelihood" id="r2_risk_likelihood">
                      <option ng-model="2.RiskLikelihood" selected value="">{{ $dpiassessment->r2_risk_likelihood }}</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <br><h6 class="text-center">R3: Data Dissappearance</h6><hr width="25%">
                <div class="form-row fix-margin">
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r3_impact_data_subjects">Impact on Data Subjects</label>
                      <textarea class="form-control" rows="3" name="r3_impact_data_subjects" id="r3_impact_data_subjects" placeholder="Potential Impact">{{ $dpiassessment->r3_impact_data_subjects }}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r3_main_threats">Main Threats</label>
                      <textarea class="form-control" rows="3" name="r3_main_threats" id="r3_main_threats" placeholder="Main Threats">{{ $dpiassessment->r3_main_threats }}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <div class="form-group">
                      <label for="r3_risk_sources">Risk Sources</label>
                      <textarea class="form-control" rows="3" name="r3_risk_sources" id="r3_risk_sources" placeholder="Risk Sources">{{ $dpiassessment->r3_risk_sources }}</textarea>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="r3_risk_severity">Risk Severity</label>
                    <select class="custom-select my-1 mr-sm-2" name="r3_risk_severity" id="r3_risk_severity">
                      <option selected value="">{{ Request::old('r3_risk_severity') ?: $dpiassessment->r3_risk_severity }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="my-1 mr-2" for="r3_risk_likelihood">Risk Likelihood</label>
                    <select class="custom-select my-1 mr-sm-2" name="r3_risk_likelihood" id="r3_risk_likelihood">
                      <option selected value="">{{ $dpiassessment->r3_risk_likelihood }}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <!-- Validation -->
            <br><h5>Validation</h5><hr>
            <form method="post" action="{{ route('dashboard.validate.dpiassessment', $dpiassessment->uuid) }}">
              <div class="row">
                <div class="col-lg-4" style="margin-left: 2%; margin-right:-5%">
                  <div class="text-center"ng-app="myApp">
                    <div ng-controller="myCtrl">
                      <div class="resized">
                        <risk-matrix data="data.risks" likelihood="data.likelihoodValues" impact="data.impactValues" template="data.riskTemplate"></risk-matrix>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="additional_measures">Additional Measures</label>
                        <textarea class="form-control {{ $errors->has('additional_measures') ? 'is-invalid' : ''}}" rows="2" name="additional_measures" id="additional_measures" placeholder="Suggest additional technical and organisational measures for future implementation.">{{ Request::old('additional_measures') ?: $dpiassessment->additional_measures }}</textarea>
                        @if ($errors->has('additional_measures'))
                          <span class="help-block">{{ $errors->first('additional_measures') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="dpo_opinion">DPO's Opinion</label>
                        <textarea class="form-control {{ $errors->has('dpo_opinion') ? 'is-invalid' : ''}}" rows="2" name="dpo_opinion" id="dpo_opinion" placeholder="Include the opinion of the designated Data Protection Officer on this DPI Assessment.">{{ Request::old('dpo_opinion') ?: $dpiassessment->dpo_opinion }}</textarea>
                        @if ($errors->has('dpo_opinion'))
                          <span class="help-block">{{ $errors->first('dpo_opinion') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-12 text-center">
                      @if ($dpiassessment->status != null)
                        <div class="btn-group" role="group">
                          <button type="button" class="btn btn-danger" disabled><i class="fas fa-fw fa-times"></i> Reject Assessment</button>
                          <button type="button" class="btn btn-success" disabled><i class="fas fa-fw fa-check"></i> Approve Assessment</button>
                        </div>
                      @else
                        <form method="post" action="{{ route('dashboard.validate.dpiassessment', $dpiassessment->uuid) }}">
                          <div class="btn-group" role="group">
                            <button type="submit" name="validate" value="reject" class="btn btn-danger"><i class="fas fa-fw fa-times"></i> Reject Assessment</button>
                            <button type="submit" name="validate" value="approve" class="btn btn-success"><i class="fas fa-fw fa-check"></i> Approve Assessment</button>
                          </div>
                          <input type="hidden" name="_token" value="{{ Session::token() }}">
                        </form>
                      @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div><br><br>

    @include('templates.partials.dashboard.footer')

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/main.min.js') }}"></script>
    <script src="{{ asset('js/active.js') }}"></script>
    <script src="{{ asset('js/tagsinput.js') }}"></script>
    <script src="{{ asset('js/angular.min.js') }}"></script>
    <script src="{{ asset('js/angular-risk-matrix.js') }}"></script>
    <script>
      var app = angular.module("myApp", ['riskMatrix']);

      app.controller("myCtrl", function($scope){
          $scope.data = {};
          
          $scope.data.risks = [
              {
                  Id: 1,
                  Title: 'Illegitimate Access to Data',
                  RiskLikelihood: '{{ $dpiassessment->r1_risk_likelihood }}',
                  RiskImpact: '{{ $dpiassessment->r1_risk_severity }}'
              },
              {
                  Id: 2,
                  Title: 'Unwanted Modification of Data',
                  RiskLikelihood: '{{ $dpiassessment->r3_risk_likelihood }}',
                  RiskImpact: '{{ $dpiassessment->r2_risk_severity }}'
              },
              {
                  Id: 3,
                  Title: 'Data Dissappearance',
                  RiskLikelihood: '{{ $dpiassessment->r3_risk_likelihood }}',
                  RiskImpact: '{{ $dpiassessment->r3_risk_severity }}'
              },
          ];
          
          $scope.data.likelihoodValues = [
              'Undefined','Negligible','Limited','Important','Maximum'
          ];
          
          $scope.data.impactValues = [
              'Undefined','Negligible','Limited','Important','Maximum'
          ];
          
          $scope.data.riskTemplate = '<div class="closed"><span ng-bind="\'R\'+item.Id"></span></div><div class="open"><div class="title" ng-bind="item.Title"></div><div ng-bind="\'Likelihood: \'+item.RiskLikelihood"></div><div ng-bind="\'Impact: \'+item.RiskImpact"></div></div>';
      });
    </script>

  </div>
</body>

</html>
