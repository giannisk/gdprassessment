<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / GDPR Assessment / View GDPR Assessment</title>

    @include('templates.partials.dashboard.stylesheet')
    <link href="{{ asset('css/gdprassessment.css') }}" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard.gdprassessment') }}">GDPR Assessment</a>
          </li>
          <li class="breadcrumb-item active">View GDPR Assessment</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>GDPR Assessment</h1>
            <hr>
          </div>
        </div>

        <!-- View GDPR Assessment -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <!-- Assessment Status -->
            @if ($gdprassessment->status == "Compliant")
            <div class="card text-white bg-success mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-check-circle"></i> Compliant</h1>
                <p class="card-text lead">Congratulations, your organisation seems to be perfectly aligned with the EU GDPR. Please refer to the following indications.</p>
              </div>
            </div>
            @elseif ($gdprassessment->status == "Semi-Compliant")
            <div class="card text-white bg-warning mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-exclamation-triangle"></i> Semi-Compliant</h1>
                <p class="card-text lead">Hmm, your organisation seems to be partially complying with the EU GDPR. Please refer to the following indications.</p>
              </div>
            </div>
            @elseif ($gdprassessment->status == "Non-Compliant")
            <div class="card text-white bg-danger mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-times-circle"></i> Non-Compliant</h1>
                <p class="card-text lead">Uh oh, it seems your organisation does not comply with the EU GDPR. Please refer to the following indications.</p>
              </div>
            </div>
            @else
            <div class="card text-white bg-info mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-question-circle"></i> Pending</h1>
                <p class="card-text lead">Hmm, we need some additional time to determine your organisation's alignment with the EU GDPR.</p>
              </div>
            </div>
            @endif

            <!-- Essential Information -->
            <h5>Essential Information</h5><hr>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="first_name">First Name</label>
                <input class="form-control" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ $gdprassessment->first_name }}">
              </div>
              <div class="form-group col-md-3">
                <label for="last_name">Last Name</label>
                <input class="form-control" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ $gdprassessment->last_name }}">
              </div>
              <div class="form-group col-md-3">
                <label for="organisation_name">Organisation Name</label>
                <input class="form-control" name="organisation_name" id="organisation_name" type="text" placeholder="Organisation Name" value="{{ $gdprassessment->organisation_name }}">
              </div>
              <div class="form-group col-md-3">
                <label for="job_title">Job Title</label>
                <input class="form-control" name="job_title" id="job_title" type="text" placeholder="Job Title" value="{{ $gdprassessment->job_title }}">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="organisation_role">Which of the following best describes the role of your organisation?</label>
                <select class="custom-select my-1 mr-sm-2" name="organisation_role" id="organisation_role">
                  <option selected value="">{{ $gdprassessment->organisation_role }}</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="EU_data_subjects">Does your organisation involve data subjects in the EU?</label>
                <select class="custom-select my-1 mr-sm-2" name="EU_data_subjects" id="EU_data_subjects">
                  <option selected value="">{{ $gdprassessment->EU_data_subjects }}</option>
                </select>
              </div>
            </div>

            <!-- Organisational Requirements -->
            <br><h5>Organisational Requirements</h5><hr>
            <div class="form-row mb-0">
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="technical_organisational_measures">Has your organisation implemented appropriate technical and organisational measures?</label>
                <select class="custom-select my-1 mr-sm-2" name="technical_organisational_measures" id="technical_organisational_measures">
                  <option selected value="">{{ $gdprassessment->technical_organisational_measures }}</option>
                </select>
                @if ($gdprassessment->technical_organisational_measures == "Yes, my organisation has implemented appropriate technical and organisational measures for the safeguarding of personal data.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisational measures to ensure the continuous protection of personal data.</div> 
                @elseif ($gdprassessment->technical_organisational_measures == "No, my organisation has not implemented appropriate technical and organisational measures for the safeguarding of personal data.")
                <div class="alert alert-danger small mb-0" role="alert"><i class="fas fa-fw fa-times"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisational measures to ensure the continuous protection of personal data.</div>
                @else
                <div class="alert alert-warning small mb-0" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisational measures to ensure the continuous protection of personal data.</div>
                @endif
              </div>
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="records_processing_activities">Does your organisation maintain records of processing activities?</label>
                <select class="custom-select my-1 mr-sm-2" name="records_processing_activities" id="records_processing_activities">
                  <option selected value="">{{ $gdprassessment->records_processing_activities }}</option>
                </select>
                @if ($gdprassessment->records_processing_activities == "Yes, my organisation maintains records of processing activities.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> Art. 30 Par. 1 GDPR stresses the necessity of maintaining records of processing activities which burdens both controllers and processors.</div>
                @elseif ($gdprassessment->records_processing_activities == "No, my organisation does not maintain records of processing activities.")
                <div class="alert alert-danger small mb-0" role="alert"><i class="fas fa-fw fa-times"></i> Art. 30 Par. 1 GDPR stresses the necessity of maintaining records of processing activities which burdens both controllers and processors.</div>
                @else
                <div class="alert alert-warning small mb-0" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 30 Par. 1 GDPR stresses the necessity of maintaining records of processing activities which burdens both controllers and processors.</div>
                @endif
              </div>
            </div>
            <div class="form-row mt-0">
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="privacy_policy">Has your organisation published an easy-to-understand privacy policy?</label>
                <select class="custom-select my-1 mr-sm-2" name="privacy_policy" id="privacy_policy">
                  <option selected value="">{{ $gdprassessment->privacy_policy }}</option>
                </select>
                @if ($gdprassessment->privacy_policy == "Yes, there is a publicly available privacy policy that explains data processing activities and guides data subjects through exercising their rights.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> Art. 13 and Art. 14 GDPR require controllers to provide data subjects with particular information wherever they obtain personal data from the data subject or from another source.</div>
                @elseif ($gdprassessment->privacy_policy == "No, there isn't such a document published anywhere.")
                <div class="alert alert-danger small mb-0" role="alert"><i class="fas fa-fw fa-times"></i> Art. 13 and 14 GDPR require controllers to provide data subjects with particular information wherever they obtain personal data from the data subject or from another source.</div>
                @else
                 <div class="alert alert-warning small mb-0" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 13 and 14 GDPR require controllers to provide data subjects with particular information wherever they obtain personal data from the data subject or from another source.</div>
                @endif
              </div>
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="data_protection_design_default">Does your organisation embrace data protection by design and by default?</label>
                <select class="custom-select my-1 mr-sm-2" name="data_protection_design_default" id="data_protection_design_default">
                  <option selected value="">{{ $gdprassessment->data_protection_design_default }}</option>
                </select>
                @if ($gdprassessment->data_protection_design_default == "Yes, my organisation embraces data protection by design and by default in accordance with Art. 25 GDPR.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> Art. 25 GDPR expects controllers to safeguard personal data right from the start and to process personal data with the highest privacy protection.</div>
                @elseif ($gdprassessment->data_protection_design_default == "No, my organisation does not embrace data protection by design and by default.")
                <div class="alert alert-danger small mb-0" role="alert"><i class="fas fa-fw fa-times"></i> Art. 25 GDPR expects controllers to safeguard personal data right from the start and to process personal data with the highest privacy protection.</div>
                @else
                <div class="alert alert-warning small mb-0" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 25 GDPR expects controllers to safeguard personal data right from the start and to process personal data with the highest privacy protection.</div>
                @endif
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="data_breach_procedure">Has your organisation established a procedure in the event of a data breach?</label>
                <select class="custom-select my-1 mr-sm-2" name="data_breach_procedure" id="data_breach_procedure">
                  <option selected value="">{{ $gdprassessment->data_breach_procedure }}</option>
                </select>
                @if ($gdprassessment->data_breach_procedure == "Yes, my organisation has established a procedure in the event of a data breach.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> In the case of a personal data breach, Art. 33 and Art. 34 GDPR expect controllers to notify the respective supervisory authority and, in certain circumstances, the data subjects.</div>
                @elseif ($gdprassessment->data_breach_procedure == "No, my organisation has not established a procedure in the event of a data breach.")
                <div class="alert alert-danger small mb-0" role="alert"><i class="fas fa-fw fa-times"></i> In the case of a personal data breach, Art. 33 and Art. 34 GDPR expect controllers to notify the respective supervisory authority and, in certain circumstances, the data subjects.</div>
                @else
                <div class="alert alert-warning small mb-0" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> In the case of a personal data breach, Art. 33 and Art. 34 GDPR expect controllers to notify the respective supervisory authority and, in certain circumstances, the data subjects.</div>
                @endif
              </div>
              <div class="form-group col-md-6">
                <label class="my-1 mr-2" for="data_protection_officer">Has your organisation appointed a data protection officer (DPO)?</label>
                <select class="custom-select my-1 mr-sm-2" name="data_protection_officer" id="data_protection_officer">
                  <option selected value="">{{ $gdprassessment->data_protection_officer }}</option>
                </select>
                @if ($gdprassessment->data_protection_officer == "Yes, my organisation has appointed a data protection officer.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> Art. 37 Par. 1 GDPR requires the controller and the processor to designate a data protection officer, in certain circumstances.</div>
                @elseif ($gdprassessment->data_protection_officer == "No, my organisation does not need to appoint a data protection officer.")
                <div class="alert alert-success small mb-0" role="alert"><i class="fas fa-fw fa-check"></i> Art. 37 Par. 1 GDPR requires the controller and the processor to designate a data protection officer, in certain circumstances.</div>
                @elseif ($gdprassessment->data_protection_officer == "No, my organisation has not appointed a data protection officer.")
                <div class="alert alert-danger small mb-0" role="alert"><i class="fas fa-fw fa-times"></i> Art. 37 Par. 1 GDPR requires the controller and the processor to designate a data protection officer, in certain circumstances.</div>
                @else
                <div class="alert alert-warning small mb-0" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 37 Par. 1 GDPR requires the controller and the processor to designate a data protection officer, in certain circumstances.</div>
                @endif
              </div>
            </div>

            <!-- Processing Requirements -->
            <br><h5>Processing Requirements</h5><hr>
@if(!count($gdprassessment->processingactivities))
            <div class="row">
              <div class="col-12 mb-3">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <p class="mb-auto">There seem to be no records of processing activities associated with this GDPR Assessment.</p>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </div>
            </div>
@else
            <div class="row">
              <div class="col-12 mb-3">
                <div class="accordion" id="accordionExample">

@foreach ($gdprassessment->processingactivities as $processingactivity)
@if($loop->iteration == 1)
                  <!-- Processing Activity No. {{ $loop->iteration }} -->
                  <div class="card">
                    <div class="card-header" id="heading{{ $loop->iteration }}">
                      <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="true" aria-controls="collapse{{ $loop->iteration }}">
                          <span class="fas fa-fw fa-database"></span> Processing Activity No. {{ $loop->iteration }}: {{ $processingactivity->processingactivity_name }}
                        </button>
                      </h5>
                    </div>
                    <div id="collapse{{ $loop->iteration }}" class="collapse show" aria-labelledby="heading{{ $loop->iteration }}" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <label for="storage_method">Storage Method</label>
                            <select class="custom-select" name="storage_method" id="storage_method">
                              <option selected value="">{{ $processingactivity->storage_method }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="type">Data Type</label>
                            <select class="custom-select" name="data_type" id="data_type">
                              <option selected value="">{{ $processingactivity->data_type }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="legal_justification">Legal Justification</label>
                            <select class="custom-select" name="legal_justification" id="legal_justification">
                              <option selected value="">{{ $processingactivity->legal_justification }}</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <label for="security_measures">Security Measures</label>
                            <select class="custom-select" name="security_measures" id="security_measures">
                              <option selected value="">{{ $processingactivity->security_measures }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="processing_principles">Processing Principles</label>
                            <select class="custom-select" name="processing_principles" id="processing_principles">
                              <option selected value="">{{ $processingactivity->processing_principles }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="data_subject_rights">Data Subject Rights</label>
                            <select class="custom-select" name="data_subject_rights" id="data_subject_rights">
                              <option selected value="">{{ $processingactivity->data_subject_rights }}</option>
                            </select>
                          </div>
                        </div>
                        @if ($processingactivity->legal_justification == "None")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 6 Par. 1 GDPR establishes six legal bases for rendering the processing lawful. You did not specify any of these legal justifications.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 6 Par. 1 GDPR establishes six legal bases for rendering the processing lawful. Your legal justification appears to be valid.</div>
                        @endif
                        @if ($processingactivity->security_measures == "Yes, my organisation has implemented some appropriate technical and organisational measures.")
                        <div class="alert alert-warning small mb-1" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisatinal measures to ensure the confidentiality, integrity, and availability of processing systems.</div>
                        @elseif ($processingactivity->security_measures == "No, my organisation has not implemented any technical and organisational measures.")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisatinal measures to ensure the confidentiality, integrity, and availability of processing systems.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisatinal measures to ensure the confidentiality, integrity, and availability of processing systems.</div>
                        @endif
                        @if ($processingactivity->processing_principles == "Yes, my organisation upholds some processing principles.")
                        <div class="alert alert-warning small mb-1" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 5 Par. 1 GDPR highlights six principles relating to the processing of personal data. Art. 5 Par. 2 GDPR holds controllers accountable for compliance with these principles.</div>
                        @elseif ($processingactivity->processing_principles == "No, my organisation does not uphold any processing principle.")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 5 Par. 1 GDPR highlights six principles relating to the processing of personal data. Art. 5 Par. 2 GDPR holds controllers accountable for compliance with these principles.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 5 Par. 1 GDPR highlights six principles relating to the processing of personal data. Art. 5 Par. 2 GDPR holds controllers accountable for compliance with these principles.</div>
                        @endif
                        @if ($processingactivity->data_subject_rights == "Yes, my organisation helps data subjects exercise some of their rights.")
                        <div class="alert alert-warning small mb-1" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 15, Art. 16, Art. 17, Art. 18, Art. 20 and Art. 21 GDPR introduce six fundamental rights of data subjects. Controllers must honour these rights, where applicable.</div>
                        @elseif ($processingactivity->data_subject_rights == "No, my organisation does not help data subjects exercise their rights.")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 15, Art. 16, Art. 17, Art. 18, Art. 20 and Art. 21 GDPR introduce six fundamental rights of data subjects. Controllers must honour these rights, where applicable.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 15, Art. 16, Art. 17, Art. 18, Art. 20 and Art. 21 GDPR introduce six fundamental rights of data subjects. Controllers must honour these rights, where applicable.</div>
                        @endif
                      </div>
                    </div>
                  </div>
@else
                  <div class="card">
                    <div class="card-header" id="heading{{ $loop->iteration }}">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="false" aria-controls="collapse{{ $loop->iteration }}">
                          <span class="fas fa-fw fa-database"></span> Processing Activity No. {{ $loop->iteration }}: {{ $processingactivity->processingactivity_name }}
                        </button>
                      </h5>
                    </div>
                    <div id="collapse{{ $loop->iteration }}" class="collapse" aria-labelledby="heading{{ $loop->iteration }}o" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <label for="storage_method">Storage Method</label>
                            <select class="custom-select" name="storage_method" id="storage_method">
                              <option selected value="">{{ $processingactivity->storage_method }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="type">Data Type</label>
                            <select class="custom-select" name="data_type" id="data_type">
                              <option selected value="">{{ $processingactivity->data_type }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="legal_justification">Legal Justification</label>
                            <select class="custom-select" name="legal_justification" id="legal_justification">
                              <option selected value="">{{ $processingactivity->legal_justification }}</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-row mb-1">
                          <div class="form-group col-md-4">
                            <label for="security_measures">Security Measures</label>
                            <select class="custom-select" name="security_measures" id="security_measures">
                              <option selected value="">{{ $processingactivity->security_measures }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="processing_principles">Processing Principles</label>
                            <select class="custom-select" name="processing_principles" id="processing_principles">
                              <option selected value="">{{ $processingactivity->processing_principles }}</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="data_subject_rights">Data Subject Rights</label>
                            <select class="custom-select" name="data_subject_rights" id="data_subject_rights">
                              <option selected value="">{{ $processingactivity->data_subject_rights }}</option>
                            </select>
                          </div>
                        </div>
                        @if ($processingactivity->legal_justification == "None")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 6 Par. 1 GDPR establishes six legal bases for rendering the processing lawful. You did not specify any of these legal justifications.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 6 Par. 1 GDPR establishes six legal bases for rendering the processing lawful. Your legal justification appears to be valid.</div>
                        @endif
                        @if ($processingactivity->security_measures == "Yes, my organisation has implemented some appropriate technical and organisational measures.")
                        <div class="alert alert-warning small mb-1" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisatinal measures to ensure the confidentiality, integrity, and availability of processing systems.</div>
                        @elseif ($processingactivity->security_measures == "No, my organisation has not implemented any technical and organisational measures.")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisatinal measures to ensure the confidentiality, integrity, and availability of processing systems.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 32 Par. 1 GDPR expects controllers and processors to implement appropriate technical and organisatinal measures to ensure the confidentiality, integrity, and availability of processing systems.</div>
                        @endif
                        @if ($processingactivity->processing_principles == "Yes, my organisation upholds some processing principles.")
                        <div class="alert alert-warning small mb-1" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 5 Par. 1 GDPR highlights six principles relating to the processing of personal data. Art. 5 Par. 2 GDPR holds controllers accountable for compliance with these principles.</div>
                        @elseif ($processingactivity->processing_principles == "No, my organisation does not uphold any processing principle.")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 5 Par. 1 GDPR highlights six principles relating to the processing of personal data. Art. 5 Par. 2 GDPR holds controllers accountable for compliance with these principles.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 5 Par. 1 GDPR highlights six principles relating to the processing of personal data. Art. 5 Par. 2 GDPR holds controllers accountable for compliance with these principles.</div>
                        @endif
                        @if ($processingactivity->data_subject_rights == "Yes, my organisation helps data subjects exercise some of their rights.")
                        <div class="alert alert-warning small mb-1" role="alert"><i class="fas fa-fw fa-exclamation-triangle"></i> Art. 15, Art. 16, Art. 17, Art. 18, Art. 20 and Art. 21 GDPR introduce six fundamental rights of data subjects. Controllers must honour these rights, where applicable.</div>
                        @elseif ($processingactivity->data_subject_rights == "No, my organisation does not help data subjects exercise their rights.")
                        <div class="alert alert-danger small mb-1" role="alert"><i class="fas fa-fw fa-times"></i> Art. 15, Art. 16, Art. 17, Art. 18, Art. 20 and Art. 21 GDPR introduce six fundamental rights of data subjects. Controllers must honour these rights, where applicable.</div>
                        @else
                        <div class="alert alert-success small mb-1" role="alert"><i class="fas fa-fw fa-check"></i> Art. 15, Art. 16, Art. 17, Art. 18, Art. 20 and Art. 21 GDPR introduce six fundamental rights of data subjects. Controllers must honour these rights, where applicable.</div>
                        @endif
                        </div>
                      </div>
                    </div>
                  </div>
@endif
@endforeach
                </div>
              </div>
            </div>
@endif
          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/gdprassessment.js') }}"></script>

      </div>
    </div>

  </body>

</html>
