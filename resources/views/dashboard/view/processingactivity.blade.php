<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / Processing Activity / View</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard.processingactivity') }}">Processing Activity</a>
          </li>
          <li class="breadcrumb-item active">View Processing Activity</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>Processing Activity</h1>
            <hr>
          </div>
        </div>

        <!-- View Processing Activity -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <!-- Assessment Status -->
            <div class="card text-white bg-info mb-3 text-center">
              <div class="card-body">
                <h1 class="card-title display-4"><i class="fas fa-file-import"></i> This record is on file!</h1>
                <p class="card-text lead">{{ config('app.name') }} helps you maintain records of processing activities. These records may also be used to conduct GDPR Assessments.</p>
              </div>
            </div>

            <!-- Processing Activity -->
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                <!-- Essential Information -->
                <h5>Essential Information</h5><hr>
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="first_name">First Name</label>
                    <input class="form-control" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ $processingactivity->first_name }}">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="last_name">Last Name</label>
                    <input class="form-control" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ $processingactivity->last_name }}">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="organisation_name">Organisation Name</label>
                    <input class="form-control" name="organisation_name" id="organisation_name" type="text" placeholder="Organisation Name" value="{{ $processingactivity->organisation_name }}">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="job_title">Job Title</label>
                    <input class="form-control" name="job_title" id="job_title" type="text" placeholder="Job Title" value="{{ $processingactivity->job_title }}">
                  </div>
                </div>
                <!-- Summary -->
                <br><h5>Summary</h5><hr>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="processing_activity_name">Processing Activity Name</label>
                    <input class="form-control" name="processing_activity_name" id="processing_activity_name" placeholder="Processing Activity Name" value="{{ $processingactivity->processing_activity_name }}">
                  </div>
                  <div class="form-group col-md-4">
                    <label for="controller_name">Controller Name</label>
                    <input class="form-control" name="controller_name" id="controller_name" placeholder="Controller Name" value="{{ $processingactivity->controller_name }}">
                  </div>
                  <div class="form-group col-md-4">
                    <label for="processor_name">Processor Name</label>
                    <input class="form-control" name="processor_name" id="processor_name" placeholder="Processor Name" value="{{ $processingactivity->processor_name }}">
                  </div>
                </div>
                <div class="form-row mb-3">
                  <div class="col-md-12">
                    <label for="processingactivity_description">Processing Activity Description</label>
                    <textarea class="form-control" name="processingactivity_description" id="processingactivity_description" placeholder="Explain the purposes of the processing, and provide a brief description of the implemented technical and organisational measures. If your organisation transfers personal data to recipients in third countries or international organisations, please elaborate further." rows="3">{{ $processingactivity->processingactivity_description }}</textarea>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="storage_method">Storage Method</label>
                    <select class="custom-select" name="storage_method" id="storage_method">
                      <option selected value="">{{ $processingactivity->storage_method }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="type">Data Type</label>
                    <select class="custom-select" name="data_type" id="data_type">
                      <option selected value="">{{ $processingactivity->data_type }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="legal_justification">Legal Justification</label>
                    <select class="custom-select" name="legal_justification" id="legal_justification">
                      <option selected value="">{{ $processingactivity->legal_justification }}</option>
                    </select>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="security_measures">Security Measures</label>
                    <select class="custom-select" name="security_measures" id="security_measures">
                      <option selected value="">{{ $processingactivity->security_measures }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="processing_principles">Processing Principles</label>
                    <select class="custom-select" name="processing_principles" id="processing_principles">
                      <option selected value="">{{ $processingactivity->processing_principles }}</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="data_subject_rights">Data Subject Rights</label>
                    <select class="custom-select" name="data_subject_rights" id="data_subject_rights">
                      <option selected value="">{{ $processingactivity->data_subject_rights }}</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>

      </div>
    </div>

  </body>

</html>
