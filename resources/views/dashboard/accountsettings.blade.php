<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / Account Settings</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Account Settings</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>Account Settings</h1>
            <hr>
          </div>
        </div>

        <!-- Account Settings -->
        <div class="row">
          <!-- Update Account Information -->
          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 mb-4">
            <div class="card">
              <div class="card-header">Update Account Information</div>
              <div class="card-body">
                <form method="post" action="{{ route('dashboard.accountsettings') }}">
                  <div class="form-group">
                    <div class="form-row">
                      <div class="col-md-6">
                        <label for="first_name">First Name</label>
                        <input class="form-control {{ $errors->has('first_name') ? 'is-invalid' : ''}}" name="first_name" id="first_name" type="text" placeholder="First Name" value="{{ Request::old('first_name') ?: Auth::user()->first_name }}">
                        @if ($errors->has('first_name'))
                          <span class="help-block">{{ $errors->first('first_name') }}</span>
                        @endif
                      </div>
                      <div class="col-md-6">
                        <label for="last_name">Last Name</label>
                        <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : ''}}" name="last_name" id="last_name" type="text" placeholder="Last Name" value="{{ Request::old('last_name') ?: Auth::user()->last_name }}">
                        @if ($errors->has('last_name'))
                          <span class="help-block">{{ $errors->first('last_name') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="form-row">
                      <div class="col-md-6">
                        <label for="password">E-mail Address</label>
                        <input class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" name="email" id="email" type="email" placeholder="E-mail Address" value="{{ Request::old('email') ?: Auth::user()->email }}">
                        @if ($errors->has('email'))
                          <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                      </div>
                      <div class="col-md-6">
                        <label for="username">Username</label>
                        <input class="form-control {{ $errors->has('username') ? 'is-invalid' : ''}}" name="username" id="username" type="text" placeholder="Username" value="{{ Auth::user()->username }}" disabled>
                        @if ($errors->has('username'))
                          <span class="help-block">{{ $errors->first('username') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="form-row">
                      <div class="col-md-6">
                        <label for="password">Password</label>
                        <input class="form-control {{ $errors->has('password') ? 'is-invalid' : ''}}" name="password" id="password" type="password" placeholder="Password" value="">
                        @if ($errors->has('password'))
                          <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                      </div>
                      <div class="col-md-6">
                        <label for="password_confirmation">Confirm Password</label>
                        <input class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : ''}}" name="password_confirmation" id="password_confirmation" type="password" placeholder="Confirm Password" value="">
                        @if ($errors->has('password_confirmation'))
                          <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="pull-right btn btn-success btn-block"><i class="fas fa-fw fa-user-edit"></i> Update Information</button>
                  </div>
                  <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form>
              </div>
            </div>
          </div>
          <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12 mb-4">
            <div class="row">
              <!-- Request Account Data -->
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                <div class="card">
                  <div class="card-header">Request Account Data</div>
                  <div class="card-body">
                    <p class="card-text">Request an archive containing your account data. We will e-mail you the archive.</p>
                    <a class="pull-right btn btn-primary btn-block" href="mailto:info@gdprassessment.io?subject=Request Account Data&body=Hi, I would like to request an archive containing my account data."><i class="fas fa-fw fa-archive"></i> Request Archive</a>
                  </div>
                </div>
              </div>
              <!-- Delete Account -->
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                <div class="card">
                  <div class="card-header">Delete Account</div>
                  <div class="card-body">
                    <p class="card-text">Permanently delete your entire user account. This action cannot be undone!</p>
                    <button id="delete "class="pull-right btn btn-danger btn-block" data-toggle="modal" data-target="#delete_account_modal"><i class="fas fa-fw fa-user-times"></i> Delete Account</button>
                  </div>
                </div>
              </div>
              <!-- Modal -->
              <div class="modal fade" id="delete_account_modal">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">Delete Account</div>
                    <form method="post" action="{{ route('dashboard.accountsettings.deleteaccount') }}">
                      <div class="modal-body">
                        <p class="modal-text">This permanently deletes your entire user account. Are you sure? This action cannot be undone!</p>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" name="delete_entire_account_data" id="delete_entire_account_data">
                          <label class="custom-control-label" for="delete_entire_account_data">In addition, delete every assessment linked to my account.</label>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fas fa-fw fa-undo-alt"></i> Undo Action</button>
                        <button type="submit" class="btn btn-danger"><i class="fas fa-fw fa-trash"></i> Delete Account</button>
                      </div>
                      <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>

      </div>
    </div>

  </body>

</html>
