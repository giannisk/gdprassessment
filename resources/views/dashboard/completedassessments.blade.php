<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.dashboard.meta')

    <!-- Title -->
    <title>{{ config('app.name') }} / Dashboard / Completed Assessments</title>

    @include('templates.partials.dashboard.stylesheet')

  </head>

  <body class="fixed-nav sticky-footer bg-primary" id="page-top">

    @include('templates.partials.dashboard.navigation')

    <!-- Content -->
    <div class="content-wrapper">
      <div class="container-fluid">

        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ route('dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Completed Assessments</li>
        </ol>

        <!-- Heading -->
        <div class="row">
          <div class="col-12">
            <h1>Completed Assessments</h1>
            <hr>
          </div>
        </div>

        <!-- Completed Assessments -->
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">

            <div class="card">
              <div class="card-header">Processing Activities</div>
              <div class="card-body">
@if(!count($processingactivities))
                <p>There seem to be no existing records of Processing Activities.</p>
@else
                <table class="table table-striped text-center">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Processing Activity</th>
                      <th scope="col">Organisation Name</th>
                      <th scope="col">Date and Time</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($processingactivities as $processingactivity)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td><a href="{{ route('dashboard.view.processingactivity', [$processingactivity->uuid]) }}">{{ $processingactivity->processingactivity_name }}</a></td>
                      <td>{{ $processingactivity->organisation_name }}</td>
                      <td>{{ $processingactivity->created_at }}</td>
                    </tr>
@endforeach
                  </tbody>
                </table>
@endif
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
            <div class="card">
              <div class="card-header">GDPR Assessments</div>
              <div class="card-body">
@if(!count($gdprassessments))
                <p>There seem to be no existing records of GDPR Assessments.</p>
@else
                <table class="table table-striped text-center">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Organisation Name</th>
                      <th scope="col">Assessment Status</th>
                      <th scope="col">Date and Time</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($gdprassessments as $gdprassessment)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td><a href="{{ route('dashboard.view.gdprassessment', [$gdprassessment->uuid]) }}">{{ $gdprassessment->organisation_name }}</a></td>
                      <td>{{ $gdprassessment->status ?: 'Pending' }}</td>
                      <td>{{ $gdprassessment->created_at }}</td>
                    </tr>
@endforeach
                  </tbody>
                </table>
@endif
              </div>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
            <div class="card">
              <div class="card-header">DPI Assessments</div>
              <div class="card-body">
@if(!count($dpiassessments))
                <p>There seem to be no existing records of DPI Assessments.</p>
@else
                <table class="table table-striped text-center">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Organisation Name</th>
                      <th scope="col">Assessment Status</th>
                      <th scope="col">Date and Time</th>
                    </tr>
                  </thead>
                  <tbody>
@foreach($dpiassessments as $dpiassessment)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td><a href="{{ route('dashboard.view.dpiassessment', [$dpiassessment->uuid]) }}">{{ $dpiassessment->organisation_name }}</a></td>
                      <td>{{ $dpiassessment->status ?: 'Pending Validation' }}</td>
                      <td>{{ $dpiassessment->created_at }}</td>
                    </tr>
@endforeach
                  </tbody>
                </table>
@endif
              </div>
            </div>
          </div>
        </div>

        @include('templates.partials.dashboard.footer')

        <!-- JavaScript -->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/main.min.js') }}"></script>
        <script src="{{ asset('js/active.js') }}"></script>

      </div>
    </div>

  </body>

</html>
