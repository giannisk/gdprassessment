<!DOCTYPE html>
<html lang="en">

  <head>

    @include('templates.partials.home.meta')

    <!-- Title -->
    <title>{{ config('app.name') }}</title>

    @include('templates.partials.home.stylesheet')
    <link href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">

  </head>

  <body>

    @include('templates.partials.home.navigation')

    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">GDPR Compliance Made Easy<br>Free Comprehensive Assessment</h1>
            <a class="btn btn-primary btn-lg" href="{{ route('auth.signup') }}">Begin Assessment</a>
          </div>
        </div>
      </div>
    </header>

    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-wallet m-auto text-primary"></i>
              </div>
              <h3>Entirely Free</h3>
              <p class="lead mb-0">No advertisments or secret tricks. Free unrestricted usage.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-layers m-auto text-primary"></i>
              </div>
              <h3>All Inclusive</h3>
              <p class="lead mb-0">Comprehensive top-down evaluation. DPIA included, too.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-check m-auto text-primary"></i>
              </div>
              <h3>Easy To Use</h3>
              <p class="lead mb-0">Guided step by step approach. Minimum effort required.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Image Showcases -->
    <section class="showcase">
      <div class="container-fluid p-0">
        <div class="row no-gutters">
          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/bg-showcase-1.jpg');"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Too Much Hassle?</h2>
            <p class="lead mb-0">The European Union has introduced the General Data Protection Regulation (GDPR) with the aim of giving citizens additional control over their personal data.</p><br>
            <p class="lead mb-0">While the regulation thoroughly describes data subject rights and the obligations of data controllers and processors, it does not offer any means of helping the latter determine their compliance level.</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/bg-showcase-2.jpg');"></div>
          <div class="col-lg-6 my-auto showcase-text">
            <h2>Not Anymore</h2>
            <p class="lead mb-0">{{ config('app.name') }} is an open-source web-based system that helps evaluate your organisation's alignment with the EU GDPR. Also lets you carry out data protection impact assessments (DPIAs) within minutes.</p><br>
            <p class="lead mb-0">This not-for-profit solution comprises Giannis Konstantinidis' diploma thesis at the University of the Aegean. It is primarily targeting small and medium-sized organisations that are striving with achieving and maintaining compliance with the EU GDPR.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Contributors Section -->
    <section class="testimonials text-center bg-light">
      <div class="container">
        <h2 class="mb-5">Contributors</h2>
        <div class="row">
          <div class="col-lg-4 offset-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="img/Giannis_Konstantinidis.jpg" alt="Giannis Konstantinidis">
              <h5>Giannis Konstantinidis</h5> 
              <p class="font-weight-light mb-0">University of the Aegean</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    @include('templates.partials.home.calltoaction')

    @include('templates.partials.home.footer')

    <!-- JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/main.min.js') }}"></script>
    <script src="{{ asset('js/active.js') }}"></script>

  </body>

</html>
