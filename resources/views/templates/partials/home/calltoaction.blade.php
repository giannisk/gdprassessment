    <!-- Call to Action -->
    <section class="call-to-action text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-4">Ready to get started?</h2>
            <a class="btn btn-primary btn-lg" href="{{ route('auth.signup') }}">Begin Assessment</a>
          </div>
        </div>
      </div>
    </section>
