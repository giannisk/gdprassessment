    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="An open-source web-based system that helps evaluate your organisation's alignment with the EU GDPR.">
    <meta name="author" content="{{ config('app.name') }}">
    <link rel="icon" href="img/favicon.ico">
