    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="mainNav">
      <a class="navbar-brand" href="{{ route('dashboard') }}">{{ config('app.name') }}</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav bg-primary" id="accordion">
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="{{ route('dashboard') }}">
              <i class="fas fa-fw fa-columns"></i>
              <span class="nav-link-text">Dashboard</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Processing Activity">
            <a class="nav-link" href="{{ route('dashboard.processingactivity') }}">
              <i class="fas fa-fw fa-database"></i>
              <span class="nav-link-text">Processing Activity</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="GDPR Assessment">
            <a class="nav-link" href="{{ route('dashboard.gdprassessment') }}">
              <i class="fas fa-fw fa-balance-scale"></i>
              <span class="nav-link-text">GDPR Assessment</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="DPI Assessment">
            <a class="nav-link" href="{{ route('dashboard.dpiassessment') }}">
              <i class="fas fa-fw fa-list"></i>
              <span class="nav-link-text">DPI Assessment</span>
            </a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Completed Assessments">
            <a class="nav-link" href="{{ route('dashboard.completedassessments') }}">
              <i class="fas fa-fw fa-copy"></i>
              <span class="nav-link-text">Completed Assessments</span>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav bg-primary sidenav-toggler">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard.accountsettings') }}">
              <i class="fas fa-fw fa-users-cog"></i>
              <span class="nav-link-text">Account Settings</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('auth.signout') }}">
              <i class="fas fa-fw fa-sign-out-alt"></i>
              <span class="nav-link-text">Sign Out</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
